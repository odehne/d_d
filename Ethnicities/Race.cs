using System.Collections.Generic;
using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.MagicAndSpells;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Tools;
using DungeonsAndDragons.Languages;
using DungeonsAndDragons.Traits;
using DungeonsAndDragons.Repositories;
using Newtonsoft.Json;
using DungeonsAndDragons.Interviews;
using DungeonsAndDragons.Utilities;

namespace DungeonsAndDragons.Ethnicities
{

    public class Race : RepositoryItem<Race> {
        public List<AbilityScoreIncrease> AbilityScoreIncreases {get; set;}
        public Attributes Attributes {get; set;}
        public Cantrips Cantrips {get; set;}
        public Alignment Alignment {get; set; }
        public List<Weapon> Weapons {get; set;}
        public EthnicityTypes EthnicityType {get; set;}
        public List<Trait> Traits {get; set;}
        public ArmorProficiencies ArmorProficiencies {get; set;}
        public WeaponProficiencies WeaponProficiencies {get; set;}
        public ToolProficiencies ToolProficiencies {get; set;}

        public CanSpeak CanSpeak {get; set;}
        public AvailableCantrips AvailableCantrips { get; set; }
        public int AllowedCantrips { get; set; }
        public Classes.Archetypes AllowedCantripClass { get; set; }
        public string Details { get; set; }

		public override string RepositoryFolder => "_ethnicities";

		public Race() {
            AbilityScoreIncreases = new List<AbilityScoreIncrease>();
            Cantrips = new Cantrips();
            Weapons = new List<Weapon>();
            ArmorProficiencies = new ArmorProficiencies();
            WeaponProficiencies = new WeaponProficiencies();
            ToolProficiencies = new ToolProficiencies();
            CanSpeak = new CanSpeak();
        }

        public override Race Load(string filename)
        {
            return JsonConvert.DeserializeObject<Race>(JsonLoader.LoadJsonFromFile(filename));
        }
    }
}