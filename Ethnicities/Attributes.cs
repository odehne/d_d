
using DungeonsAndDragons.Ethnicities.Sizes;

namespace DungeonsAndDragons.Ethnicities
{
    public class Attributes {
        
        public Size Size {get; set;}
        public int Speed {get; set;}

        public Attributes(Size size, int speed)
        {
            Size = size;
            Speed = speed;
        }

        public override string ToString()
        {
            return $"Größe: {Size.Name}, Geschwindingkeit: {Speed} ft.";
        }
    }
}