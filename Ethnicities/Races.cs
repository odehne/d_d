using System.Collections.Generic;
using System.Linq;

namespace DungeonsAndDragons.Ethnicities
{
    public class Races : List<Race> {

        public Race GetRace(EthnicityTypes ethnicityType) {
            return this.FirstOrDefault(x=>x.EthnicityType.Equals(ethnicityType));
        }

    }
}