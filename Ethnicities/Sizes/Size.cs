using System;
using System.Collections.Generic;
using System.IO;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Interviews;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;

namespace DungeonsAndDragons.Ethnicities.Sizes
{
	//Tiny	2� by 2� ft.	Imp, Sprite
	//Small   5 by 5 ft.Giant Rat, Goblin
	//Medium	5 by 5 ft.Orc, Werewolf
	//Large   10 by 10 ft.Hippogriff, Ogre
	//Huge    15 by 15 ft.Fire Giant, Treant
	//Gargantuan	20 by 20 ft.or larger  Kraken, Purple Worm

	public class Size : RepositoryItem<Size> {
        public Tuple<double, double> Space {get; set;}
        public string Examples {get; set;}
        public int Height { get; set; }

		public override string RepositoryFolder => "_sizes";

		public override Size Load(string filename)
		{
			return JsonConvert.DeserializeObject<Size>(JsonLoader.LoadJsonFromFile(filename));

		}
	}
}