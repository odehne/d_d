using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons.Ethnicities.Sizes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum SizeCategories {
        Small,
        Medium,
        Large,
        Huge,
        Gargantuan,
        Tiny
    }

}