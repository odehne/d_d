using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons.Ethnicities
{

    [JsonConverter(typeof(StringEnumConverter))]
    public enum EthnicityTypes {
        Unborn = 0,
        Elf = 10,
        DarkElf = 11,
        HighElf = 12,
        WoodElf = 13,
        HalfElf = 14,
        Human = 20,
        Halfling = 30,
        Stout = 31,
        LightFoot = 32,
        Dwarf = 40,
        HillDwarf = 41,
        MountainDwarf = 42,
        Gnome = 50,
        ForestGnome = 51,
        RockGnome = 52,
        HalfOrk = 60,
        Tiefling = 70, 
        DragonBorn = 80
     
      }
}