using System.Collections.Generic;

namespace DungeonsAndDragons
{
	
    public class NamedList : List<string> {

        public NamedList() {

        }

        public NamedList(string name, params string[] items) {
            this.Name = name;
            this.AddRange(items);
        }

        public string Name {get; set;}

        public override string ToString()
        {
            var ret = "";
            foreach (var item in this)
            {
                ret = ret + item + ",";
            }
            return ret.TrimEnd(',');
        }
    }

}
