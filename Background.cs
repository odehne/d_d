using DungeonsAndDragons.Traits;

namespace DungeonsAndDragons
{
    public class Background {
        public string Name {get; set;}
        public string Details {get; set;}
        public Trait PersonalGoal {get; set;}
        public Trait Alignment {get; set;}
            
    }

}
