using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Interviews;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using DungeonsAndDragons.Utilities;

namespace DungeonsAndDragons.Traits
{

    public class Trait : RepositoryItem<Trait> {
        public bool IsBad {get; set;}
        public string Description {get; set;}

		public override string RepositoryFolder => "_traits";

		public Trait(string id, bool isBad, string name, string description)
        {
            Id= id;
            IsBad = isBad;
            Name = name;
            Description = description;
        }

        public Trait()
        {
        }

		public override Trait Load(string filename)
		{
			return JsonConvert.DeserializeObject<Trait>(JsonLoader.LoadJsonFromFile(filename));
		}
	}

}
