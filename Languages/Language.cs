using System.IO;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Interviews;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;

namespace DungeonsAndDragons.Languages
{
    public class Language : RepositoryItem<Language>
    {
        public Language()
        {
        }

        public Language(string id, string name)
        {
            Id = id;
            Name = name;
        }

		public override string RepositoryFolder => "_languages";

		public override Language Load(string filename)
		{
            return JsonConvert.DeserializeObject<Language>(JsonLoader.LoadJsonFromFile(filename));
        }

        public override string ToString()
        {
            return $"{Name} ({Id})";
        }

    }    
}

