using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons.Languages
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LanguageCategories {
        Common,
        Dwarvish,
        UnderCommon,
        Elvish
    }
}