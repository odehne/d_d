namespace DungeonsAndDragons
{
    public class Challenge
    {
        public double Modifier {get; set;}
        public int ExperiencePoints {get; set;}
        
        public Challenge(double modifier, int xp)
        {
            Modifier = modifier;
            ExperiencePoints = xp;
        }

        public override string ToString()
        {
            return $"Challenge: {Modifier} / {ExperiencePoints} EXP.";
        }
    }
}