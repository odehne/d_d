using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum DiceType {
        d2,
        d4,
		d6,
	    d8,
        d10,
		d12,
		d20,
		d30,
		d50,
		d60
	}

}
