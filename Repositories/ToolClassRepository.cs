﻿using DungeonsAndDragons.Tools;
using System.Collections.Generic;
using System.Linq;

namespace DungeonsAndDragons.Repositories
{
	public class ToolClassRepository : Repository<ToolClass>
	{
		public const string FolderName = "_toolClasses";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new ToolClass();
				AddItem(itm.Load(fil));
			}
		}

		public ToolClass GetProperty(string id)
		{
			var lst = new List<ToolClass>(Items.OfType<ToolClass>());
			return lst.FirstOrDefault(x => x.Id.Equals(id));
		}


	}
}