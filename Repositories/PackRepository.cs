﻿using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Tools;
using System.Collections.Generic;
using System.Text;

namespace DungeonsAndDragons.Repositories
{
	public class PackRepository : Repository<Pack>
	{
		public const string FolderName = "_packs";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new Pack();
				AddItem(itm.Load(fil));
			}
		}

	}
}