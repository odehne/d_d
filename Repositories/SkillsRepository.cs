﻿using DungeonsAndDragons.Classes;

namespace DungeonsAndDragons.Repositories
{
	public class SkillsRepository : Repository<Skill>
	{
		public const string FolderName = "_skills";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new Skill();
				AddItem(itm.Load(fil));
			}
		}

	}
}