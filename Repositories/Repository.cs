using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DungeonsAndDragons.Repositories
{

    public abstract class Repository<T> 
    {

        public const string RootFolder = "C:\\git\\priv\\d_d\\repo";
       
		public List<IRepositoryItem<T>> SortedItems
		{
			get {
				return Items.OrderBy(x => x.Name).ToList();
			}
		}

        public List<IRepositoryItem<T>> Items {get; set;}
        protected Repository()
        {
            Items = new List<IRepositoryItem<T>>();
        }

        public abstract void Load();

        public List<IRepositoryItem<T>> GetItems(){
            return Items;
        }

        public IRepositoryItem<T> GetItem(string name){
            return Items.FirstOrDefault(x=>x.Name.Equals(name, System.StringComparison.CurrentCultureIgnoreCase));
        }

        public void AddItem(IRepositoryItem<T> item) {
			if (!string.IsNullOrEmpty(item.Name))
			{
				if (!Items.Any(x => x.Name.Equals(item.Name)))
				{
					Items.Add(item);
				}
			}
		}
        
        public bool RemoveItem(IRepositoryItem<T> item)
        {
            if(Items.Any()){
                for (int i = Items.Count -1; i > 0; i--)
                {
                    if(item.Name.Equals(item.Name)) {
                        Items.Remove(item);
                        return true;
                    }
                }
            }
            return false;
        } 

		public void Save(string fld)
		{
			foreach (var item in Items)
			{
				item.Save();
			}
			
		}
        
        public void UpdateItem(IRepositoryItem<T> item)
        {
            for(int i = 0; i< Items.Count; i++)
            {
                if(Items[i].Name.Equals(item.Name, System.StringComparison.CurrentCultureIgnoreCase)) {
                    Items[i] = item;
                    return;
                }
            }
        } 

		public IEnumerable<string> GetFiles(string fld)
		{
			Directory.CreateDirectory(fld);
			return Directory.GetFiles(fld);
		}
	}
}