using System.IO;
using DungeonsAndDragons.Classes;

namespace DungeonsAndDragons.Repositories
{
    public class ClassRepository : Repository<CharacterClass>
    {
        public const string FolderName = "_classes";
        public override void Load()
        {
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
            {
                var itm = new CharacterClass();
                AddItem(itm.Load(fil));
            }
        }
    }
}