using System;
using System.Collections.Generic;
using System.IO;
using DungeonsAndDragons.Arms;

namespace DungeonsAndDragons.Repositories
{

	public class WeaponRepository : Repository<Weapon>
    {
        public const string FolderName = "_weapons";
        public override void Load()
        {
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
            {
                var itm = new Weapon();
                AddItem(itm.Load(fil));
            }
        }

		public List<Weapon> GetWeapons(string propertyName)
		{
			var lst = new List<Weapon>();
			foreach (Weapon item in Items)
			{
				foreach(Property prop in item.WeaponProperties)
				{
					if(prop.Name.Equals(propertyName, StringComparison.CurrentCultureIgnoreCase))
					{
						lst.Add(item);
					}
				}
			}
			return lst;
		}

		public List<Weapon> GetWeaponsByClass(string className)
		{
			var lst = new List<Weapon>();
			foreach (Weapon item in Items)
			{
				if (item.Class.Name.Equals(className, StringComparison.CurrentCultureIgnoreCase))
					lst.Add(item);
			}
			return lst;
		}
    }
}