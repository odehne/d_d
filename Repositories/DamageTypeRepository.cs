﻿using DungeonsAndDragons.Arms;

namespace DungeonsAndDragons.Repositories
{


	public class DamageTypeRepository : Repository<DamageType>
	{
		public const string FolderName = "_damagetypes";

		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new DamageType();
				AddItem(itm.Load(fil));
			}
		}

		public DamageType GetSelected(int selectedIndex)
		{
			var i = 1;
			foreach (var item in Items)
			{
				if (i == selectedIndex)
					return (DamageType)item;
				i++;
			}
			return null;
		}

	}
}