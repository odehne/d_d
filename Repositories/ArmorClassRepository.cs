﻿using DungeonsAndDragons.Arms;
using System.Collections.Generic;
using System.Linq;

namespace DungeonsAndDragons.Repositories
{

	public class ArmorClassRepository : Repository<ArmorClass>
	{
		public const string FolderName = "_armorClasses";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new ArmorClass();
				AddItem(itm.Load(fil));
			}
		}

		public ArmorClass GetProperty(string id)
		{
			var lst = new List<ArmorClass>(Items.OfType<ArmorClass>());
			return lst.FirstOrDefault(x => x.Id.Equals(id));
		}


	}
}