﻿using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;

namespace DungeonsAndDragons.Repositories
{
	public class Rarity : RepositoryItem<Rarity>
	{
		public override string RepositoryFolder => "_rarities";

		public override Rarity Load(string filename)
		{
			return JsonConvert.DeserializeObject<Rarity>(JsonLoader.LoadJsonFromFile(filename));
		}
	}
}