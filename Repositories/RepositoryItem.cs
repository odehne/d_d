using Newtonsoft.Json;
using System;
using System.IO;

namespace DungeonsAndDragons.Repositories
{
    public interface IRepositoryItem<T>
    {
        string Name { get; set; }
        string Id { get; set; }
        T Load(string filename);
        void Save();
		string RepositoryFolder { get; }
	}

    public abstract class RepositoryItem<T> : IRepositoryItem<T>
    {
		public const string RootFolder = "C:\\git\\priv\\d_d\\repo";
		public abstract string RepositoryFolder { get; }
		[JsonProperty("Name")]
		public string Name { get; set; }
		[JsonProperty("Id")]
		public string Id { get; set; }

        public void Save()
        {
			var filename = $"{RootFolder}\\{RepositoryFolder}\\{Id}.json";
			var jset = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Auto };
            var content = JsonConvert.SerializeObject(this, Formatting.Indented, jset);
            using (var writer = new StreamWriter(filename))
            {
                writer.WriteLine(content);
                writer.Close();
            }
        }

        public abstract T Load(string filename);

    }
}