using System;
using System.Collections.Generic;
using System.IO;
using DungeonsAndDragons.Arms;

namespace DungeonsAndDragons.Repositories
{

	public class ArmorRepository : Repository<Armor>
    {
        public const string FolderName = "_armor";
        public override void Load()
        {
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
            {
                var itm = new Armor();
                AddItem(itm.Load(fil));
            }
        }

		public List<Armor> GetArmorsByClass(string className)
		{
			var lst = new List<Armor>();
			foreach (Armor item in Items)
			{
				if (item.Class.Name.Equals(className, StringComparison.CurrentCultureIgnoreCase))
					lst.Add(item);
			}
			return lst;
		}
	}
}