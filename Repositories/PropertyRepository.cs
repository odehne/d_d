﻿using DungeonsAndDragons.Arms;
using System.Collections.Generic;
using System.Linq;

namespace DungeonsAndDragons.Repositories
{

	public class PropertyRepository : Repository<Property>
	{
		public const string FolderName = "_properties";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new Property();
				AddItem(itm.Load(fil));
			}
		}

		public Property GetProperty(string id)
		{
			var lst = new List<Property>(Items.OfType<Property>());
			return lst.FirstOrDefault(x => x.Id.Equals(id));
		}

		
	}
}