﻿using DungeonsAndDragons.Classes;
using System.Linq;

namespace DungeonsAndDragons.Repositories
{
	public class BackpackItemsRepository : Repository<BackpackItem>
	{
		public const string FolderName = "_items";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new BackpackItem();
				AddItem(itm.Load(fil));
			}
		}

		public BackpackItem GetItem(string id, int amount = 1)
		{
			var itm = (BackpackItem)Items.FirstOrDefault(x => x.Id.Equals(id));
			itm.Amount = amount;
			return itm;
		}

	}
}