﻿using DungeonsAndDragons.Arms;
using System.Collections.Generic;
using System.Linq;

namespace DungeonsAndDragons.Repositories
{

	public class WeaponClassRepository : Repository<WeaponClass>
	{
		public const string FolderName = "_weaponClasses";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new WeaponClass();
				AddItem(itm.Load(fil));
			}
		}

		public WeaponClass GetProperty(string id)
		{
			var lst = new List<WeaponClass>(Items.OfType<WeaponClass>());
			return lst.FirstOrDefault(x => x.Id.Equals(id));
		}


	}
}