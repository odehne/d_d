using System.IO;
using System.Linq;
using System.Text;
using DungeonsAndDragons.Tools;

namespace DungeonsAndDragons.Repositories
{
    public class ToolsRepository : Repository<Tool>
    {
        public const string FolderName = "_tools";
        public override void Load()
        {
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
            foreach (var fil in fils)
            {
                var itm = new Tool();
                AddItem(itm.Load(fil));
            }
        }

	}
}