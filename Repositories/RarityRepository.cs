﻿using System;
using System.Collections.Generic;
using System.Linq;
using DungeonsAndDragons.Arms;

namespace DungeonsAndDragons.Repositories
{
	public class RarityRepository : Repository<Rarity>
	{
		public const string FolderName = "_rarities";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new Rarity();
				AddItem(itm.Load(fil));
			}
		}

		public Rarity GetRarity(string v)
		{
			var lst = new List<Rarity>(Items.OfType<Rarity>());
			return lst.FirstOrDefault(x => x.Id.Equals(v));
		}
	}
}