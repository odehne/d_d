using System.IO;
using DungeonsAndDragons.MagicAndSpells;

namespace DungeonsAndDragons.Repositories
{
    public class SpellsRepository : Repository<Spell>
    {
        public const string FolderName = "_spells";
        public override void Load()
        {
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
            {
                var itm = new Spell();
                AddItem(itm.Load(fil));
            }
        }
    }
}