using System.IO;
using DungeonsAndDragons.Languages;

namespace DungeonsAndDragons.Repositories
{
    public class LanguagesRepository : Repository<Language>
    {
        public const string FolderName = "_languages";
        public override void Load()
        {
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
            {
                var itm = new Language();
                AddItem(itm.Load(fil));
            }
        }
    }
}