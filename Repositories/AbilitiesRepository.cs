﻿using DungeonsAndDragons.Abilities;

namespace DungeonsAndDragons.Repositories
{

	public class AbilitiesRepository : Repository<Ability>
	{
		public const string FolderName = "_abilities";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new Ability();
				AddItem(itm.Load(fil));
			}
		}

		public override string ToString()
		{
			var value = "STR\tDEX\tCON\tINT\tWIS\tCHA\n";
			foreach (var ab in Items)
			{
				var itm = (Ability)ab;
				value = $"{value}{itm.BaseValue}({itm.Modifier})\t";
			}
			value.TrimEnd('\t');
			return value;
		}
	}
}