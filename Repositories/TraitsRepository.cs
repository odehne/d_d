﻿using DungeonsAndDragons.Traits;

namespace DungeonsAndDragons.Repositories
{
	public class TraitsRepository : Repository<Trait>
	{
		public const string FolderName = "_traits";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new Trait();
				AddItem(itm.Load(fil));
			}
		}
	}
}