using System.IO;
using DungeonsAndDragons.Ethnicities;

namespace DungeonsAndDragons.Repositories
{

	public class RaceRepository : Repository<Race>
    {
        public const string FolderName = "_ethnicities";
        public override void Load()
        {
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
            {
                var itm = new Race();
                AddItem(itm.Load(fil));
            }
        }
    }
}