﻿using DungeonsAndDragons.Ethnicities.Sizes;

namespace DungeonsAndDragons.Repositories
{
	public class SizeRepository : Repository<Size>
	{
		public const string FolderName = "_sizes";
		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new Size();
				AddItem(itm.Load(fil));
			}
		}
	}
}