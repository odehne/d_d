using System.Collections.Generic;
using System.Linq;

namespace DungeonsAndDragons
{
    public class DiceBag : List<Die>
    {
        public override string ToString()
        {
            var ret = "";
            var diceDic = new Dictionary<DiceType, int>();
            foreach (var dice in this)
            {
                if(!diceDic.Keys.Contains(dice.Type)){
                    diceDic.Add(dice.Type, 1);
                }else{
                    diceDic[dice.Type]++;
                }
            }
            foreach (var item in diceDic)
            {
                ret = $"{ret}{item.Value}x{item.Key}+";
            }

            return ret.TrimEnd('+');
        }
    }

}
