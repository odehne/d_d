using DungeonsAndDragons.Arms;
using System.Text;

namespace DungeonsAndDragons
{
    public class Die {
        public DiceType Type {get; set;} 
       
        public Die(){
            Type = DiceType.d6;
        }
        public Die(DiceType dt){
            Type =dt;
        }
    }

}
