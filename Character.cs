using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Ethnicities;
using Newtonsoft.Json;

namespace DungeonsAndDragons
{

    public class NPC : Character
    {
      
    }

    public class PC : Character
    {
      
    }

    public class Character {
       
        public CharacterClass Class {get; set;}

        public Race Race {get; set;}
   
        public Background Background { get; set; }

        public string Name {get; set;}
        
        public int Age {get; set; }

        public Alignment Alignment {get; set;}

        public int ExperiencePoints {get; set;}

        public List<Ability> Abilities {get; set;}

        public List<Skill> Skills {get; set;}

        public int PassiveWisdomOrPerception {get; set;}
        public int ArmorClass {get; set;}
        public int Initiative {get; set;}
        public int Speed {get; set;}

        public HitPoints HitPoints {get; set;}

        public NamedList Proficiencies {get; set;}
        
        public string PersonalityTraits {get; set;}
        public string Ideals {get; set;}

        public string Bonds {get; set;}
        public string Flaws {get; set;}
      
        public Challenge Challenge {get; set;}

        public Character()
        {
            //Race = Program.Races.GetRace(EthnicityTypes.HalfOrk);
            //Class  = Program.Classes.FirstOrDefault(x=>x.Name=="Barbar");
            Abilities = new List<Ability>();
            HitPoints = new HitPoints();
            Proficiencies = new NamedList() { Name = "Proficiencies" };
            Skills = new List<Skill>();
            Challenge = new Challenge(1, 1);
        }


        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
        
            sb.AppendLine($"{Name}");
            //sb.AppendLine($"{Race.Name} ({Class.Name}), {Background.Alignment.Name}");
            sb.AppendLine(Class.ToString());
            sb.AppendLine();
            sb.AppendLine("STR\tDEX\tCON\tINT\tWIS\tCHA");
            sb.AppendLine(Abilities.ToString());
            if(Challenge!=null){
                sb.AppendLine(Challenge.ToString());
            }
            sb.AppendLine();
          
            return sb.ToString();
        }

        public void Save(string filename)
        {
            var jset = new JsonSerializerSettings() { TypeNameHandling = TypeNameHandling.Auto };
            var content = JsonConvert.SerializeObject(this, Formatting.Indented, jset);
            using(var writer = new System.IO.StreamWriter(filename)){
                writer.WriteLine(content);
                writer.Close();
            }
        }
    } 

}
