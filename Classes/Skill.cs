﻿using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;

namespace DungeonsAndDragons.Classes
{

	public class Skill : RepositoryItem<Skill>
	{
		public override string RepositoryFolder => "_skills";
		public bool Effective { get; set; }

		public int Modifier { get; set; }
		public string Ability { get; set; }

		public Skill(string name, string id)
		{
			Name = name;
			Id = id;
		}

		public Skill()
		{
		}

		public override Skill Load(string filename)
		{
			return JsonConvert.DeserializeObject<Skill>(JsonLoader.LoadJsonFromFile(filename));
		}
	}
}