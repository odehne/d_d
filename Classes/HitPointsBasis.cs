﻿using DungeonsAndDragons.Abilities;

namespace DungeonsAndDragons.Classes
{
	public class HitPointsBasis
	{
		public int BaseValue { get; set; }
		public Ability Ability { get; set; }

		public int MaxModifierValue { get; set; }

		public override string ToString()
		{
			string maxVal = (MaxModifierValue > 0) ? "(max " + MaxModifierValue + ")" : "";
			if (Ability != null)
			{
				return $"{BaseValue} {Ability.Name} {maxVal}";
			}
			else
			{
				return $"{BaseValue} {maxVal}";
			}
		}
	}
}