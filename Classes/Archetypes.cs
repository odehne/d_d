using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons.Classes
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Archetypes {
        Barberian,
        Bard,
        Cleric,
        Druid,
        Fighter,
        Monk,
        Paladin,
        Ranger,
        Rogue,
        Sorcerer,
        Warlock,
        Wizard,
        Monster
    }

}
