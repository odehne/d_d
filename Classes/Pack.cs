﻿using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Tools;
using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonsAndDragons.Classes
{
	public class Pack : RepositoryItem<Pack>
	{
		public override string RepositoryFolder => "_packs";

		public Pack(string name, string id)
		{
			Id = id;
			Name = name;
			Items = new List<BackpackItem>();
		}

		public Pack()
		{
			Items = new List<BackpackItem>();
		}

		public List<BackpackItem> Items { get; set; }

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.AppendLine($"{Name} ({Id})");
			foreach (BackpackItem itm in Items)
			{
				sb.AppendLine(itm.ToString());
			}
			return sb.ToString();
		}

		public override Pack Load(string filename)
		{
			return JsonConvert.DeserializeObject<Pack>(JsonLoader.LoadJsonFromFile(filename));
		}
	}
}