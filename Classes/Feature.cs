﻿namespace DungeonsAndDragons.Classes
{
	public class Feature
	{
		public string Name { get; set; }
		public string Id { get; set; }
		public string Description { get; set; }
	}
}