using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Tools;
using System.Collections.Generic;

namespace DungeonsAndDragons.Classes
{
    public class Equipment {
		public List<Weapon> WeaponsToChoose { get; set; }
		public List<WeaponClass> WeaponClassesToChoose { get; set; }
		public List<Armor> ArmorToChoose { get; set; }
		public List<ArmorClass> ArmorClassesToChoose { get; set; }
		public List<Pack> PacksToChoose { get; set; }
		public List<Tool> ToolToChoose { get; set; }
		public List<ToolClass> ToolClassesToChoose { get; set; }
	}

}
