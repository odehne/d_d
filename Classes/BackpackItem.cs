﻿using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;

namespace DungeonsAndDragons.Classes
{
	public class BackpackItem : RepositoryItem<BackpackItem>
	{
		public override string RepositoryFolder => "_items";

		public int Amount { get; set; }

		public BackpackItem(string name, string id, int amount = 1)
		{
			Id = id;
			Name = name;
			Amount = amount;
		}

		public BackpackItem()
		{
		}

		public override string ToString()
		{
			if(Amount > 1)
			{
				return $"\t{Amount}x {Name} ({Id})";
			}
			return $"\t{Name} ({Id})";
		}

		public override BackpackItem Load(string filename)
		{
			return JsonConvert.DeserializeObject<BackpackItem>(JsonLoader.LoadJsonFromFile(filename));
		}
	}
}