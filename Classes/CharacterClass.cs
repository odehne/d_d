using System.Collections.Generic;
using System.Text;
using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.MagicAndSpells;
using DungeonsAndDragons.Repositories;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Tools;

namespace DungeonsAndDragons.Classes
{
    public class CharacterClass : RepositoryItem<CharacterClass> {
        public string Details { get; set; }
        public Die HitDice { get; set; }

		public HitPointsBasis HitPointsFirstLevel { get; set; }
		public HitPointsBasis HitPointsHigherlevels { get; set; }

        public int SpellsKnown {get; set;}
        public int CantripsKnown {get; set;} 
     
		public List<Weapon> WeaponsToChooseFrom { get; set; }
		public List<Skill> SkillsToChooseFrom { get; set; }
		public int HowManySkills { get; set; }

		public int ProficiencyBonus { get; set; }
		public List<Feature> Features { get; set; }

		public List<ArmorClass> ArmorClasses { get; set; }
		public List<WeaponClass> WeaponClasses { get; set; }
		public List<Weapon> Weapons { get; set; }
		public List<ToolClass> ToolClasses { get; set; }
		public List<Ability> SavingThrows { get; set; }

		public override string RepositoryFolder => "_classes";

		public Equipment Equipment { get; internal set; }

		public CharacterClass(){
         
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"Klasse: {Name} ({Id}) Level: 1");
            
            return sb.ToString();
        }

        public override CharacterClass Load(string filename)
        {
            return JsonConvert.DeserializeObject<CharacterClass>(JsonLoader.LoadJsonFromFile(filename));
        }
    } 

}
