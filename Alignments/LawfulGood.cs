namespace DungeonsAndDragons.Ethnicities
{
    public class LawfulGood : Alignment
    {
        public LawfulGood()
        {
            Name = "Gesetzestreu und gut";
            Example = "Kreuzritter, Königswache, Paladin";
            Description = "A lawful good character acts as a good person is expected or required to act.";
        }
        
        public override string ToString()
        {
            return $"Ausrichtung: {Name}";
        }
    }
}