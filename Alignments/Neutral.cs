namespace DungeonsAndDragons.Ethnicities
{
    public class Neutral : Alignment
    {
        public Neutral()
        {
            Name = "Neutral";
            Example = "Unendschlossen";
            Description = "A neutral character does what seems to be a good idea. She doesn't feel strongly one way or the other when it comes to good vs. evil or law vs. chaos. Most neutral characters exhibit a lack of conviction or bias rather than a commitment to neutrality. Such a character thinks of good as better than evil-after all, she would rather have good neighbors and rulers than evil ones. Still, she's not personally committed to upholding good in any abstract or universal way.";
        }
        
    }
}