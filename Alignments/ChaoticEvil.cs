namespace DungeonsAndDragons.Ethnicities
{
    public class ChaoticEvil : Alignment
    {
        public ChaoticEvil()
        {
            Name = "Gesetzlos und böse";
            Example = "Zerstörer, Dämon";
            Description = "A chaotic evil character does whatever his greed, hatred, and lust for destruction drive him to do. He is hot-tempered, vicious, arbitrarily violent, and unpredictable. If he is simply out for whatever he can get, he is ruthless and brutal. If he is committed to the spread of evil and chaos, he is even worse. Thankfully, his plans are haphazard, and any groups he joins or forms are poorly organized. Typically, chaotic evil people can be made to work together only by force, and their leader lasts only as long as he can thwart attempts to topple or assassinate him.";
        }
        
    }
}