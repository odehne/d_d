namespace DungeonsAndDragons.Ethnicities
{
    public class LawfulNeutral : Alignment
    {
        public LawfulNeutral()
        {
            Name = "Gesetzestreu und neutral";
            Example = "Richter";
            Description = "A lawful neutral character acts as law, tradition, or a personal code directs her. Order and organization are paramount to her.";
        }
        
    }
}