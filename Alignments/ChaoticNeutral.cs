namespace DungeonsAndDragons.Ethnicities
{
    public class ChaoticNeutral : Alignment
    {
        public ChaoticNeutral()
        {
            Name = "Gesetzlos neutral";
            Example = "Freigeist";
            Description = "A chaotic neutral character follows his whims. He is an individualist first and last.";
        }
        
    }
}