namespace DungeonsAndDragons.Ethnicities
{
    public class Alignment {
        public string Name {get; set;}
        public string Description {get; set; }
        public string Example {get; set; }

        public override string ToString()
        {
            return $"Ausrichtung: {Name}";
        }
    }
}