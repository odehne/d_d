namespace DungeonsAndDragons.Ethnicities
{
    public class NeutralEvil : Alignment
    {
        public NeutralEvil()
        {
            Name = "Gesetzestreu und böse";
            Example = "Übeltäter";
            Description = "A neutral evil villain does whatever she can get away with. She is out for herself, pure and simple.";
        }
        
    }
}