namespace DungeonsAndDragons.Ethnicities
{
    public class NeutralGood : Alignment
    {
        public NeutralGood()
        {
            Name = "Neutral und gut";
            Example = "Wohltäter, Mäzen";
            Description = "A neutral good character does the best that a good person can do.";
        }
        
        public override string ToString()
        {
            return $"Ausrichtung: {Name}";
        }
    }
}