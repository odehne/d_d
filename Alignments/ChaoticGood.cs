namespace DungeonsAndDragons.Ethnicities
{
    public class ChaoticGood : Alignment
    {
        public ChaoticGood()
        {
            Name = "Gesetzlos und gut";
            Example = "Rebel, Pirat";
            Description = "A chaotic good character acts as his conscience directs him with little regard for what others expect of him.";
        }
        
        public override string ToString()
        {
            return $"Ausrichtung: {Name}";
        }
    }
}