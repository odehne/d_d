namespace DungeonsAndDragons.Ethnicities
{
    public class LawfulEvil : Alignment
    {
        public LawfulEvil()
        {
            Name = "Gesetzestreu und böse";
            Example = "Herrscher";
            Description = "A lawful evil villain methodically takes what he wants within the limits of his code of conduct without regard for whom it hurts.";
        }
        
    }
}