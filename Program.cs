﻿using System;
using System.Collections.Generic;
using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Ethnicities;
using DungeonsAndDragons.Ethnicities.Sizes;
using DungeonsAndDragons.Traits;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Interviews;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;
using DungeonsAndDragons.Tools;

namespace DungeonsAndDragons
{
    public class Program
    {
		public static RarityRepository Rarities { get; set; }

		//https://dnd.wizards.com/articles/features/character_sheets
		//https://www.dndbeyond.com/classes
		//https://www.dndbeyond.com/sources/basic-rules/equipment
		//http://gdnd.wikidot.com/race:elf
		public static AbilitiesRepository Abilities { get; set; }
		public static DamageTypeRepository DamageTypes { get; set; }
		public static PropertyRepository Properties { get; set; }
		public static WeaponRepository Weapons {get; set;}
        public static ClassRepository Classes {get; set; }
        public static RaceRepository Races {get; set; }
        public static ToolsRepository Tools {get; set;}
		public static ToolClassRepository ToolClasses { get; set; }
		public static ArmorRepository Armor {get; set;}
        public static SpellsRepository Spells {get; set;}
		public static SkillsRepository Skills { get; set; }
		public static ArmorClassRepository ArmorClasses { get; set; }
		public static WeaponClassRepository WeaponClasses { get; set; }
		public static PackRepository Packs { get; set; }
		public static BackpackItemsRepository BackpackItems { get; set; }
		public static CurrencyRepository CurrencyUnits { get; set; }
		public static SizeRepository Sizes {get; set; }

        //public static BasicAbilities Abilities {get; set;}
        static void Main(string[] args)
        {
			Reload();

		
		//	DamageTypes.Save("_damagTypes");

			Classes = new ClassRepository();
			Classes.Load();	
            Races= new RaceRepository();
			Races.Load();
            Tools= new ToolsRepository();
			Tools.Load();
            Armor= new ArmorRepository();
			Armor.Load();
            Spells= new SpellsRepository();
			Spells.Load();

			RunMainInterview();

            var si = new RaceInterview();
            si.RunInterview();
            si.TheRace.Save();
            Console.WriteLine($"C:\\git\\priv\\d_d\\repo\\_ethnicities\\{si.TheRace.Id}.json saved.");

            Environment.Exit(0);
                var md = new Race();
            md.Name = "Elf (Dunkelelf)";
            md.EthnicityType = EthnicityTypes.DarkElf;
            var size = new Size();
            size.Id = "Medium";
            size.Space = new Tuple<double, double>(5.0,5.0);
            size.Height = 6;
            size.Name = "mittelgroß";

            md.Attributes = new Attributes(size, 30);            
            
            md.Traits = new List<Trait>();

			md.Alignment = new Alignment();
            md.Alignment.Name = "Elfische Ausrichtung";
            md.Alignment.Description = "Elfen lieben die Freiheit, Abwechslung und Selbstentfaltung. Sie neigen daher ein wenig zum Chaos. Anderen Freiheit zu schützen halten sie genauso für wichtig wie ihre eigene.";

            md.AbilityScoreIncreases.Add(new AbilityScoreIncrease {AbilityType=AbilityType.Dexterity, Bonus=2});
            md.AbilityScoreIncreases.Add(new AbilityScoreIncrease {AbilityType=AbilityType.Charisma, Bonus=1});
            
            md.WeaponProficiencies.Name = "Waffentraining für Elfen (Elven Weapon Training)";
         

        }

		public static void Reload()
		{
			InitToolClasses();
			LoadBackpackItems();
			LoadPacks();
			LoadWeaponClasses();
			InitSkills();
			LoadArmorClasses();
			LoadRarities();
			InitSizes();
			LoadDamagaeTypes();
			LoadCurrencyUnits();
			LoadAbilities();
			LoadProperties();
			LoadWeapons();
		}

		public static void RunMainInterview()
		{
			MainInterview.RunInterview();
		}

		private static void LoadProperties()
		{
			Properties = new PropertyRepository();
			Properties.Load();
		}

		private static void LoadDamagaeTypes()
		{
			DamageTypes = new DamageTypeRepository();
			DamageTypes.Load();
		}
		private static void LoadRarities()
		{
			Rarities = new RarityRepository();
			Rarities.Load();
		}

		public static void LoadCurrencyUnits()
		{
			CurrencyUnits = new CurrencyRepository();
			CurrencyUnits.Load();
		}

		public static void LoadWeapons()
		{
			Weapons = new WeaponRepository();
			Weapons.Load();
		}

		public static void LoadAbilities()
		{
			Abilities = new AbilitiesRepository();
			Abilities.Load();
		}

		private static void LoadWeaponClasses()
		{
			WeaponClasses = new WeaponClassRepository();
			WeaponClasses.Load();
		}

		private static void LoadPacks()
		{
			Packs = new PackRepository();
			Packs.Load();
			
		}

		private static void InitToolClasses()
		{
			//https://roll20.net/compendium/dnd5e/Tools#content
			ToolClasses = new ToolClassRepository();
			ToolClasses.Items.Add(new ToolClass("Werkzeuge für Handwerker", "Artisan's tools"));
			ToolClasses.Items.Add(new ToolClass("Musikinstrumente", "Musical instruments"));
			ToolClasses.Items.Add(new ToolClass("Spielzeug", "Gaming sets"));
			ToolClasses.Save("_toolClasses");
		}
		private static void LoadBackpackItems()
		{
			BackpackItems = new BackpackItemsRepository();
			BackpackItems.Load();
			
		}

		private static void InitSkills()
		{
			Skills = new SkillsRepository();
			Skills.Load();
		}

		private static void LoadArmorClasses()
		{
			ArmorClasses = new ArmorClassRepository();
			ArmorClasses.Load();
		}

	

	private static void InitSizes()
        {
            Sizes = new SizeRepository();
            Sizes.Items.Add(JsonConvert.DeserializeObject<Size>(JsonLoader.LoadJsonFromFile("C:\\git\\priv\\d_d\\repo\\_sizes\\Gargantuan.json")));
            Sizes.Items.Add(JsonConvert.DeserializeObject<Size>(JsonLoader.LoadJsonFromFile("C:\\git\\priv\\d_d\\repo\\_sizes\\huge.json")));
            Sizes.Items.Add(JsonConvert.DeserializeObject<Size>(JsonLoader.LoadJsonFromFile("C:\\git\\priv\\d_d\\repo\\_sizes\\large.json")));
            Sizes.Items.Add(JsonConvert.DeserializeObject<Size>(JsonLoader.LoadJsonFromFile("C:\\git\\priv\\d_d\\repo\\_sizes\\medium.json")));
            Sizes.Items.Add(JsonConvert.DeserializeObject<Size>(JsonLoader.LoadJsonFromFile("C:\\git\\priv\\d_d\\repo\\_sizes\\small.json")));
            Sizes.Items.Add(JsonConvert.DeserializeObject<Size>(JsonLoader.LoadJsonFromFile("C:\\git\\priv\\d_d\\repo\\_sizes\\tiny.json")));
			Sizes.Save("_sizes");
        }  
      
      

        public static NPC CreateGoblin() {
            
            var m = new NPC();
            m.Name = "Imrie, der Kleine";
            //m.Race = new Race { Name="Menschlich", Details = "klein, goblinisch" };
            //m.Background = new Background {Alignment = new FeatureOrTrait {Name="Neutral böse"} };
            m.Class = new Monster("Monster");
            m.ArmorClass = 15;
            m.Speed = 30;
            m.HitPoints = new HitPoints(7);
            m.Abilities.Add(new Ability("Strength", "Stärke", 8));
            m.Abilities.Add(new Ability("Dexterity", "Geschicklichkeit", 14));
            m.Abilities.Add(new Ability("Constitution", "Konstitution", 10));
            m.Abilities.Add(new Ability("Intelligence", "Intelligenz", 10));
            m.Abilities.Add(new Ability("Wisdom", "Weisheit", 8));
            m.Abilities.Add(new Ability("Charisma", "Charisma", 8));
            m.PassiveWisdomOrPerception = 9;

            m.Proficiencies = new NamedList("Fertigkeiten", "Sehen im Dunkeln (60ft)");
           // m.Skills.Add(new Skill { Name="Heimlichkeit", Modifier=+6, Ability="(GE)"});
            m.Challenge = new Challenge(.25, 50);

            return m;
        }

        public static PC CreateRagnar() {
            var c = new PC();

            c.Name = "Ragnar der Orkschlächter";
            c.Abilities.Add(new Ability("Strength", "Stärke", 16));
            c.Abilities.Add(new Ability("Dexterity", "Geschicklichkeit", 9));
            c.Abilities.Add(new Ability("Constitution", "Konstitution", 15));
            c.Abilities.Add(new Ability("Intelligence", "Intelligenz", 11));
            c.Abilities.Add(new Ability("Wisdom", "Weisheit", 13));
            c.Abilities.Add(new Ability("Charisma", "Charisma", 14));
            c.PassiveWisdomOrPerception = 13;
            var bla = Classes.GetItem("Fighter");
            
            c.ArmorClass = 17;
            c.Initiative = -1;
            c.Speed = 30;
            c.HitPoints = new HitPoints(12);
			c.Proficiencies = new NamedList("Fertigkeiten", "Alle Rüstungen", "Alle Shilde", "Einfache Waffen", "Brutale Waffen", "Kartenspiele");
           
            return c;
        }
    }

}

//private static void InitCurrencyUnits()
//{


// var darkVision = new Spell("DarkVision");
// darkVision.CastingTime = 1;
// darkVision.RangeType = RangeType.Touch;
// darkVision.Range = 0;
// darkVision.Duration = 8;
// darkVision.AllowedClasses = new List<Archetypes> { Archetypes.Druid, Archetypes.Ranger, Archetypes.Sorcerer, Archetypes.Wizard };
// darkVision.Save("C:\\git\\priv\\d_d\\repo\\_spells\\darkVision.json");



// md.FeaturesAndTraits.Add(new FeatureOrTrait(FeatureCategory.KeenSenses, false, "Geschärfte Sinne", "Verbesserte Wahrnehmung (Perception Skill Bonus)."));
// md.FeaturesAndTraits.Add(new FeatureOrTrait(FeatureCategory.FeyAncestry, false, "Geschärfte Sinne", "Verbesserte Abwehr (Saving Throw) gegen Charme Zauber."));
// md.FeaturesAndTraits.Add(new FeatureOrTrait(FeatureCategory.Trance, false, "Trance", "Elfen schlafen nicht, sondern fallen in tiefe Meditation, wobei sie halb bei Bewußtsein bleiben. Das Wort in Common (der allgemeinen Sprache) für meditieren in Trance. Eine vier stündiger Trance eines Elfen entsrpicht einem durchschnittlichen Schlaf eines Menchen von acht Stunden. Träume im Trance trainieren die Sinne obendrein."));
// md.FeaturesAndTraits.Add(new FeatureOrTrait(FeatureCategory.SuperiorDarkVision, false, "Überragene Darkvision", "Stark verbesserte Sicht im Dunkeln. Bei schwachem oder stark gedimmten Licht siehst du noch bis 120ft passabel."));
// md.FeaturesAndTraits.Add(new FeatureOrTrait(FeatureCategory.SunlightSensitivity, true, "Empfindlichkeit gegen Sonnenlicht", "Du hast einen Nachteil bei Angriffswürfen (attack roll disadvantage) und auf Weisheit bei (Perception) Proben."));

////Entertainer's
//BackpackItems.Items.Add(new BackpackItem("Rucksack", "Backpack"));
//BackpackItems.Items.Add(new BackpackItem("Schlafsack", "Bedroll"));
//BackpackItems.Items.Add(new BackpackItem("Kostüm", "Costume"));
//BackpackItems.Items.Add(new BackpackItem("Kerze", "Candle"));
//BackpackItems.Items.Add(new BackpackItem("Tagesration", "Ration per day"));
//BackpackItems.Items.Add(new BackpackItem("Wasserflasche", "Waterskin"));
//BackpackItems.Items.Add(new BackpackItem("Maskerade", "Disguise Kit"));

////Diplomat's packs
//BackpackItems.Items.Add(new BackpackItem("Kiste", "Chest"));
//BackpackItems.Items.Add(new BackpackItem("Schutzhülle für Karten und Schriftrollen", "Cases for maps and scrolls"));
//BackpackItems.Items.Add(new BackpackItem("Ein Satz feiner Kleidung", "Set of fine cloths"));
//BackpackItems.Items.Add(new BackpackItem("Eine Flasche Tinte", "Bottle of ink"));
//BackpackItems.Items.Add(new BackpackItem("Federkiel", "Ink pen"));
//BackpackItems.Items.Add(new BackpackItem("Lampe", "Lamp"));
//BackpackItems.Items.Add(new BackpackItem("Öl Fläschchen", "Flask of oil"));
//BackpackItems.Items.Add(new BackpackItem("Parfüm Phiole", "Vial of perfume"));
//BackpackItems.Items.Add(new BackpackItem("Seite Papier", "Sheets of paper"));
//BackpackItems.Items.Add(new BackpackItem("Siegelwachs", "Sealing wax"));
//BackpackItems.Items.Add(new BackpackItem("Seife", "Soap"));

////Explorer's packs
//BackpackItems.Items.Add(new BackpackItem("Essgeschirr", "Mess kit"));
//BackpackItems.Items.Add(new BackpackItem("Zunder und Feuerstein", "Tinderbox"));
//BackpackItems.Items.Add(new BackpackItem("Fackel", "Torch"));
//BackpackItems.Items.Add(new BackpackItem("Kletterseil", "Hempen rope"));

////Dungeoneer's Pack
//BackpackItems.Items.Add(new BackpackItem("Brechstange", "Crowbar"));
//BackpackItems.Items.Add(new BackpackItem("Hammer zum Klettern", "Hammer"));
//BackpackItems.Items.Add(new BackpackItem("Kletterhaken", "Piton"));

//BackpackItems.Save("_items");

//var diplomatsPack = new Pack("Rucksack für Diplomaten", "Diplomat’s pack");
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Chest"));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Cases for maps and scrolls", 2));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Set of fine cloths"));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Bottle of ink"));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Ink pen"));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Lamp"));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Flask of oil", 2));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Sheets of paper", 5));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Vial of perfume"));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Sealing wax"));
//	diplomatsPack.Items.Add(BackpackItems.GetItem("Soap"));

//var entertainerPack = new Pack("Rucksack für Entertainer", "Entertainer’s pack");
//	entertainerPack.Items.Add(BackpackItems.GetItem("Backpack"));
//	entertainerPack.Items.Add(BackpackItems.GetItem("Bedroll"));
//	entertainerPack.Items.Add(BackpackItems.GetItem("Costume", 2));
//	entertainerPack.Items.Add(BackpackItems.GetItem("Candle", 5));
//	entertainerPack.Items.Add(BackpackItems.GetItem("Ration per day", 5));
//	entertainerPack.Items.Add(BackpackItems.GetItem("Waterskin"));
//	entertainerPack.Items.Add(BackpackItems.GetItem("Disguise Kit"));

//var explorersPack = new Pack("Rucksack für Entdecker", "Explorer’s pack");
//	explorersPack.Items.Add(BackpackItems.GetItem("Backpack"));
//	explorersPack.Items.Add(BackpackItems.GetItem("Bedroll"));
//	explorersPack.Items.Add(BackpackItems.GetItem("Mess kit"));
//	explorersPack.Items.Add(BackpackItems.GetItem("Torch", 10));
//	explorersPack.Items.Add(BackpackItems.GetItem("Tinderbox"));
//	explorersPack.Items.Add(BackpackItems.GetItem("Ration per day", 10));
//	explorersPack.Items.Add(BackpackItems.GetItem("Waterskin"));
//	explorersPack.Items.Add(BackpackItems.GetItem("Hempen rope", 50));

//var dungeoneersPack = new Pack("Rucksack für Höhlenforscher", "Dungeoneer’s pack");
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Backpack"));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Crowbar"));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Hammer"));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Piton", 10));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Torch", 10));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Tinderbox"));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Ration per day", 10));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Waterskin"));
//	dungeoneersPack.Items.Add(BackpackItems.GetItem("Hempen rope", 50));

//Packs.AddItem(dungeoneersPack);
//Packs.AddItem(explorersPack);
//Packs.AddItem(entertainerPack);
//Packs.AddItem(diplomatsPack);

//Packs.Save("_packs");

//	CurrencyUnits = new CurrencyRepository();
//	CurrencyUnits.Items.Add(new CurrencyUnit {UnitType = CurrencyUnitTypes.Copper, Id = "Copper", Name = "Kupfer", CpMultiplier = 1.0, SpMultiplier = 10.0, EpMultiplier = 50.0, GpMultiplier = 100.0, PpMultiplier = 1000.0 });
//	CurrencyUnits.Items.Add(new CurrencyUnit {UnitType = CurrencyUnitTypes.Silver, Id = "Silver", Name = "Silber", CpMultiplier = 0.1, SpMultiplier = 1, EpMultiplier = 5, GpMultiplier = 10.0, PpMultiplier = 100.0 });
//	CurrencyUnits.Items.Add(new CurrencyUnit {UnitType = CurrencyUnitTypes.Electrum, Id = "Electrum", Name = "Electrum", CpMultiplier = 0.02, SpMultiplier = 0.2, EpMultiplier = 1.0, GpMultiplier = 2.0, PpMultiplier = 20.0 });
//	CurrencyUnits.Items.Add(new CurrencyUnit {UnitType = CurrencyUnitTypes.Gold, Id = "Gold", Name = "Gold", CpMultiplier = 0.01, SpMultiplier = 0.1, EpMultiplier = 0.5, GpMultiplier = 1.0, PpMultiplier = 10.0 });
//	CurrencyUnits.Items.Add(new CurrencyUnit {UnitType = CurrencyUnitTypes.Platinum, Id = "Platinum", Name = "Platin", CpMultiplier = 0.001, SpMultiplier = 0.01, EpMultiplier = 0.05, GpMultiplier = 0.1, PpMultiplier = 1.0 });
//
//c.Skills.Add(new Skill { Name="Akrobatik"			  , Modifier=-1, Ability="(GE)"});
//c.Skills.Add(new Skill { Name="Arkanes"			,   Modifier=0, Ability="(IN)"});
//c.Skills.Add(new Skill { Name="Athletik"			, Modifier=5, Ability="(ST)"});
//c.Skills.Add(new Skill { Name="Auftreten"				, Modifier=2, Ability="(CH)"});
//c.Skills.Add(new Skill { Name="Einschüchterung"		, Modifier=2, Ability="(CH)"});
//c.Skills.Add(new Skill { Name="Einsicht"				, Modifier=1, Ability="(WE)"});
//c.Skills.Add(new Skill { Name="Fingerfertigkeit"			, Modifier=-1, Ability="(GE)"});
//c.Skills.Add(new Skill { Name="Heilkunde"					, Modifier=1, Ability="(WE)"});
//c.Skills.Add(new Skill { Name="Geschichte"											, Modifier=2, Ability="(IN)"});
//c.Skills.Add(new Skill { Name="Natur"					, Modifier=0, Ability="(IN)"});
//c.Skills.Add(new Skill { Name="Religion"					, Modifier=0, Ability="(IN)"});
//c.Skills.Add(new Skill { Name="Überzeugung"					, Modifier=4, Ability="(CH)"});
//c.Skills.Add(new Skill { Name="Untersuchung"					, Modifier=0, Ability="(IN)"});
//c.Skills.Add(new Skill { Name="Täuschung"					, Modifier=2, Ability="(CH)"});
//c.Skills.Add(new Skill { Name="Tierumgang"					, Modifier=1, Ability="(IN)"});
//c.Skills.Add(new Skill { Name="Wahrnehmung"					, Modifier=3, Ability="(WE)"});
//c.Skills.Add(new Skill { Name="Wildnisleben"					, Modifier=1, Ability="(WE)"});
//c.Skills.Add(new Skill { Name="Heimlichkeit"					, Modifier=-1, Ability="(GE)"});
// md.WeaponProficiencies.Add(new WeaponProficiency(WeaponType.Rapier));
// md.WeaponProficiencies.Add(new WeaponProficiency(WeaponType.ShortSword));
// md.WeaponProficiencies.Add(new WeaponProficiency(WeaponType.CrossBow));

// md.ArmorProficiencies.Name = "Rüstungstraining für Zwerge (Dwarven Armor Training)";
// md.ArmorProficiencies.Add(new ArmorProficiency(ArmorCategories.Light));
// md.ArmorProficiencies.Add(new ArmorProficiency(ArmorCategories.Medium)); [DsmInstallationPath]

// md.ToolProficiencies.Name = "Umgang mit Werkzeugen";
// md.ToolProficiencies.Add(new ToolProficiency(ToolCategories.Smiths));
// md.ToolProficiencies.Add(new ToolProficiency(ToolCategories.Brewers));
// md.ToolProficiencies.Add(new ToolProficiency(ToolCategories.Masons));

// md.CanSpeak.Add(new Language(LanguageCategories.Common, "Allgemein"));
// md.CanSpeak.Add(new Language(LanguageCategories.Elvish, "Sprache der Elfen"));
// md.AllowedCantrips = 1;
// md.AvailableCantrips = new AvailableCantrips {Level = 0, Class=Archetypes.Wizard };
// md.AvailableCantrips.AddRange(new List<string> {"Acid Splash", "Blade Ward", "Chill touch", "Dancing Lights", "Fire Bolt", "Friends", "Mage Hand", "Mending", "Message", "Minor Illusion", "Poison Spray", "Prestidigitation", "Ray of Frost", "Shoking Grasp", "True Strike"});

// md.Save("C:\\git\\priv\\d_d\\repo\\_ethnicities\\elf-woodelf.json");

// var interview = new PCInterview();
// interview.RunInterview();
// Console.WriteLine(interview.Player.ToString());
//}
