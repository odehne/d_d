using System;
using System.Collections.Generic;

namespace DungeonsAndDragons.Abilities
{
    public class AbilityScores {

        public Dictionary<int, int> AbilityScoreModifiers {get; set;}
        
        public AbilityScores() {
            AbilityScoreModifiers = new Dictionary<int, int>();
            AbilityScoreModifiers.Add(0, -5);
            AbilityScoreModifiers.Add(1, -5);
            AbilityScoreModifiers.Add(2, -4);
            AbilityScoreModifiers.Add(3, -4);
            AbilityScoreModifiers.Add(4, -3);
            AbilityScoreModifiers.Add(5, -3);
            AbilityScoreModifiers.Add(6, -2);
            AbilityScoreModifiers.Add(7, -2);
            AbilityScoreModifiers.Add(8, -1);
            AbilityScoreModifiers.Add(9, -1);
            AbilityScoreModifiers.Add(10, 0);
            AbilityScoreModifiers.Add(11, 0);
            AbilityScoreModifiers.Add(12, 1);
            AbilityScoreModifiers.Add(13, 1);
            AbilityScoreModifiers.Add(14, 2);
            AbilityScoreModifiers.Add(15, 2);
            AbilityScoreModifiers.Add(16, 3);
            AbilityScoreModifiers.Add(17, 3);
            AbilityScoreModifiers.Add(18, 4);
            AbilityScoreModifiers.Add(19, 4);
            AbilityScoreModifiers.Add(20, 6);
        } 

        public int CalculateMofier(int abilityScore) {
            return (abilityScore-10)/2;
        }

    }

}