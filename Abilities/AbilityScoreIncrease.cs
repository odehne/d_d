namespace DungeonsAndDragons.Abilities
{
    public class AbilityScoreIncrease
    {
        public AbilityType AbilityType { get; set;}
        public int Bonus {get; set;}
    }
}