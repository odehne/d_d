using System.IO;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Interviews;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;

namespace DungeonsAndDragons.Abilities
{

    public class Ability : RepositoryItem<Ability> {
     
        public int BaseValue {get; set;}
        public int Modifier {get; set;}

        public int Bonus {get; set;}

		public override string RepositoryFolder => "_abilities";

		public Ability(){

        }

        public Ability(string id, string name)
        {
            Id = id;
            Name = name;
        }

        public Ability(string id, string name, int baseValue)
        {
            BaseValue = baseValue;
			Id = id;
			Name = name;
        }

        
        public override string ToString()
        {
            return $"Name: {Name}({Id}) Basiswert: {BaseValue}, Modifikator: {Modifier}, Bonus: {Bonus}";
        }

        public override Ability Load(string filename)
        {
            return JsonConvert.DeserializeObject<Ability>(JsonLoader.LoadJsonFromFile(filename));
        }
    }

}
