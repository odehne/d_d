using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons.Abilities
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AbilityType
    {
            Strength,
            Dexterity,
            Constitution, 
            Intelligence,
            Wisdom,
            Charisma 
    }
}