using System;
using System.IO;
using System.Net;
using System.Text;

namespace DungeonsAndDragons.Utilities 
{
    public class JsonLoader {
        public static string LoadJsonFromFile(string path)
        {
            if(string.IsNullOrEmpty(path)) throw new ArgumentException("File path cannot be empty.");
            if(!File.Exists(path)) throw new FileNotFoundException("File not found [" + path + "]");
            using (var sr = new StreamReader(path))
            {
                return sr.ReadToEnd();
            }
        }

        public static string LoadJsonFromUrl(string url)
        {
            if (string.IsNullOrEmpty(url)) throw new ArgumentException("The download URL must have a value.");
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                wc.Headers.Add("Cache-Control", "no-cache, no-store, must-revalidate");
                wc.Headers.Add("Pragma", "no-cache");
                wc.Headers.Add("Expires", "0");
                return wc.DownloadString(url);
            }
        }
    }
    
}