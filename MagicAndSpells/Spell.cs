using System.Collections.Generic;
using System.IO;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Interviews;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;

namespace DungeonsAndDragons.MagicAndSpells
{

    public class Spell : RepositoryItem<Spell> {
        
        public string Summary {get; set;}

        public List<Component> Components {get; set;}

        public int CastingTime {get; set; } // 1 Action

        public RangeType RangeType {get; set; } // Touch
        public int Range {get; set;} // Fernkampf

        public int Duration {get; set; } // 8 hours

        public Die HitDice {get; set;}

        public List<Archetypes> AllowedClasses {get; set;}

		public override string RepositoryFolder => "_spells";

		public Spell(string id, string name)
        {
           Name = name;
           Components = new List<Component>();
           AllowedClasses = new List<Archetypes>();
        }

        public Spell()
        {
        }

        public override string ToString()
        {
            return $"{Name}\t{Summary}.";
        }

        public override Spell Load(string filename)
        {
            return JsonConvert.DeserializeObject<Spell>(JsonLoader.LoadJsonFromFile(filename));
        }
    }
}