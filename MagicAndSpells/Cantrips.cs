using System.Collections.Generic;

namespace DungeonsAndDragons.MagicAndSpells
{
    public class Cantrips : List<Spell>{
        
        public int AllowedCantrips {get; set;}
        public void AddSpell(Spell spell){
            this.Add(spell);
        }

        public override string ToString()
        {
            var ret = "";
            foreach (var spell in this)
            {
                ret = $"{ret}{spell.ToString()}\n";
            }
            return ret;
        }
    }
}