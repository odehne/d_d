using System.Collections.Generic;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Ethnicities;

namespace DungeonsAndDragons.MagicAndSpells
{
    public class AvailableCantrips : List<string>  {
        public int Level {get; set;}
        public Archetypes Class {get; set;}
    }
}