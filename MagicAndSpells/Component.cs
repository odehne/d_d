using System.Collections.Generic;

namespace DungeonsAndDragons.MagicAndSpells
{

    public class Components : List<Component>
    {
        public override string ToString()
        {
            return base.ToString();
        }
    }

    public class Component
    {
        public ComponentTypes ComponentType {get; set;}
        public string Name {get; set;}

        public string Material {get; set;}
    }
}