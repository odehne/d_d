namespace DungeonsAndDragons.MagicAndSpells
{
    public enum RangeType {
        Touch = 1,
        Distance = 2
    }
}