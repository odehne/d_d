using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons.MagicAndSpells
{
   [JsonConverter(typeof(StringEnumConverter))]
    public enum ComponentTypes
    {
        Verbal,
        Somatic,
        Material
    }
}