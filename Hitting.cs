using System.Collections.Generic;

namespace DungeonsAndDragons
{
    public class HitPoints {
        public int MaxHitPoints {get; set;}
        public int CurrentHitPoints {get; set;}
        public int TemporaryHitPoints {get; set;}
        public int Successes {get; set;}
        public int Failures {get; set;}

        public HitPoints() {
        }

        public HitPoints(int maxHP)  {
          
            MaxHitPoints = maxHP;
        }
    }

}
