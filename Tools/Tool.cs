using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Interviews;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using DungeonsAndDragons.Utilities;
using DungeonsAndDragons.Arms;

namespace DungeonsAndDragons.Tools
{

	public class Tool : RepositoryItem<Tool>
    {
		public override string RepositoryFolder => "_tools";

		public double Weight { get; set; }

		public Cost Cost { get; set; }
		public ToolClass Class { get; internal set; }

		public Tool()
		{
			Cost = new Cost();
			Class = new ToolClass();
		}

		public override Tool Load(string filename)
        {
            return JsonConvert.DeserializeObject<Tool>(JsonLoader.LoadJsonFromFile(filename));
        }
    }

}