﻿using DungeonsAndDragons.Repositories;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;
using System.Text;

namespace DungeonsAndDragons.Tools
{
	public class ToolClass : RepositoryItem<ToolClass>
	{
		public override string RepositoryFolder => "_toolClasses";

		public ToolClass(string name, string id)
		{
			Id = id;
			Name = name;
		}

		public ToolClass()
		{
		}

		public string ToString(ToolsRepository tools)
		{
			var sb = new StringBuilder();
			sb.AppendLine($"{Name} ({Id})");
			foreach (Tool tool in tools.Items)
			{
				if (tool.Class.Id == Id)
					sb.AppendLine($"\t{tool.Name} ({tool.Id})");
			}
			return sb.ToString();
		}

		public override ToolClass Load(string filename)
		{
			return JsonConvert.DeserializeObject<ToolClass>(JsonLoader.LoadJsonFromFile(filename));
		}
	}

}