using DungeonsAndDragons.Repositories;

namespace DungeonsAndDragons.Tools
{
    public class ToolProficiency
    {
        public IRepositoryItem<Tool> Categories {get; set;}

        public ToolProficiency(IRepositoryItem<Tool> categories)
        {
            Categories = categories;
        }
    }    
}

