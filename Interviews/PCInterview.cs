using System;
using DungeonsAndDragons.Ethnicities;
using DungeonsAndDragons.Classes;
using System.Collections.Generic;

namespace DungeonsAndDragons.Interviews
{

    public class PCInterview : Interview {

        public PC Player {get; set;}

        public PCInterview()
        {
            Player = new PC();
        }

        public void RunInterview(){
            // PrintHeader("Charakter", "Spieler Charakter", "Erschaffe einen neuen Spieler Charakter");
            // Player.Name = GetName(Player.Name);
            // ChooseARace(Program.Races);
            // ChooseAClass(Program.Classes);
            // if(Player.Race.AllowedCantrips > 0 ) {
            //     Console.WriteLine($"Dein Charakter hat {Player.Race.AllowedCantrips} freie Zaubersprüche zur Auswhal.");
            //     for (int i = 0; i < Player.Race.AllowedCantrips -1; i++)
            //     {
            //         var si = new SpellInterview(Program.Classes);
            //         si.RunInterview();
            //         if(si.TheSpell!=null) {
            //             si.TheSpell.Save($"C:\\git\\priv\\d_d\\repo\\_spells\\{si.TheSpell.Name}.json");
            //             Player.Race.Cantrips.Add(si.TheSpell);
            //         }
            //     }
            // }
            // Player.Save($"C:\\git\\priv\\d_d\\repo\\_players\\{Player.Name}.json");
            // DetermineAbilityScores();
            // DescribeYourCharacter();
            // ComeTogether();
            //SetBackground();
        }

        private void ComeTogether()
        {
            throw new NotImplementedException();
        }

        private void DescribeYourCharacter()
        {
            throw new NotImplementedException();
        }

        private void DetermineAbilityScores()
        {
            throw new NotImplementedException();
        }

        private void ChooseARace(Races races)
        {
            var raceId = 0;
            Console.WriteLine($"Verfügbare Ethnien und ethnischer Hintergrund" );
            for (int i = 1; i <= races.Count; i++)
            {
                if(Player.Race.Name.Equals(races[i-1].Name))
                    raceId = i;
                Console.WriteLine($"{i}. {races[i-1].Name}");
            }

            var selectedNumber = GetNumber(raceId, $"Ethnie wählen [{Player.Race.Name}]", false);
            Player.Race = races[selectedNumber-1];
        }

       

        public void ChooseAClass(List<CharacterClass> classes){
            Console.WriteLine($"Verfügbare Charakter Klassen" );
            var classId = 0;
            for (int i = 1; i <= classes.Count; i++)
            {
                if(Player.Class.Name.Equals(classes[i-1].Name))
                    classId = i;
                Console.WriteLine($"{i}. {classes[i-1].Name}");
            }
            var selectedNumber = GetNumber(classId, $"Klasse wählen [{Player.Class.Name}]", false);
            Player.Class = classes[selectedNumber-1];
        } 

        public override string ToString()
        {
            return base.ToString();
        }
    }
}