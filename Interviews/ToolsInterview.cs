﻿using System;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Tools;

namespace DungeonsAndDragons.Interviews
{
	public class ToolsInterview : Interview
	{
		public Tool TheTool { get; set; }

		public ToolsInterview(Tool theTool)
		{
			TheTool = theTool;
		}

		public ToolsInterview()
		{
			TheTool = new Tool();
		}

		public void RunInterview()
		{
			TheTool.Class = GetToolClass(TheTool.Class);
			TheTool.Id = GetLine(TheTool.Id, "Name des Werkzeugs/Instruments", "(engl.)");
			TheTool.Name = GetLine(TheTool.Name, "Name der Werkzeugs/Instruments", "(deutsch)");
			TheTool.Weight = GetDouble(TheTool.Weight, "Wie schwer ist das Werkzeug (2.0 lb)");
			TheTool.Cost = GetCost(TheTool.Cost);
		}

		private Cost GetCost(Cost cost)
		{
			cost.Value = GetDouble(cost.Value, "Wie viel kostet das Werkzeug");
			Console.WriteLine("Währungseinheiten");
			for (int i = 0; i < Program.CurrencyUnits.Items.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.CurrencyUnits.Items[i].Name}({Program.CurrencyUnits.Items[i].Id})");
			}
			var selected = GetNumber(0, "Währungseinheit wählen", false);
			cost.Unit = (CurrencyUnit)Program.CurrencyUnits.Items[selected];
			return cost;
		}

		private ToolClass GetToolClass(ToolClass toolClass)
		{
			Console.WriteLine("Werkzeug- und Musikinstrumenteklassen");
			for (int i = 0; i < Program.ToolClasses.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.ToolClasses.SortedItems[i].Name}({Program.ToolClasses.SortedItems[i].Id})");
			}
			var selected = GetNumber(0, "Werkzeug/Instrumentenklasse wählen", false);
			toolClass = (ToolClass)Program.ToolClasses.SortedItems[selected];
			return toolClass;
		}

		internal static Tool SelectTool(ToolsRepository tools)
		{
			Console.WriteLine("Gespeicherte Werkzeuge/Instrumente");
			for (int i = 0; i < tools.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {tools.SortedItems[i].Name} ({tools.SortedItems[i].Id})");
			}
			var selected = GetNumber(0, "Wähle ein Werkzeug/Instrument", false);
			return (Tool)tools.SortedItems[selected];
		}
	}
}