﻿using System;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Repositories;

namespace DungeonsAndDragons.Interviews
{
	public class BackpackItemInterview : Interview
	{
		public BackpackItem TheItem { get; set; }

		public BackpackItemInterview(BackpackItem theItem)
		{
			TheItem = theItem;
		}

		public BackpackItemInterview()
		{
			TheItem = new BackpackItem();
		}

		public void RunInterview()
		{
			TheItem.Id = GetLine(TheItem.Id, "Name des Gegenstandes", "(engl.)");
			TheItem.Name = GetLine(TheItem.Name, "Name des Gegenstandes", "(deutsch)");
		}

		internal static BackpackItem SelectBackpackItem(BackpackItemsRepository items)
		{
			Console.WriteLine("Gespeicherte Gegenstände");
			for (int i = 0; i < items.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {items.SortedItems[i].Name} ({items.SortedItems[i].Id})");
			}
			var selected = GetNumber(0, "Wähle einen Gegenstand", false);
			return (BackpackItem)items.SortedItems[selected];
		}
	}
	
}