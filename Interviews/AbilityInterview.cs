﻿using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.Repositories;
using System;

namespace DungeonsAndDragons.Interviews
{
	public class AbilityInterview : Interview
	{
		public Ability TheAbility { get; set; }

		public AbilityInterview(Ability theAbility)
		{
			TheAbility = theAbility;
		}

		public AbilityInterview()
		{
			TheAbility = new Ability();
		}

		public void RunInterview()
		{
			TheAbility.Id = GetLine(TheAbility.Id, "Fähigkeitsname", "(engl.)");
			TheAbility.Name = GetLine(TheAbility.Name, "Fähigkeitsname", "(deutsch)");
			TheAbility.BaseValue = GetNumber(TheAbility.BaseValue, "Basiswert", true);
			TheAbility.Bonus = GetNumber(TheAbility.Bonus, "Fähigkeitsbonus", true);
			TheAbility.Modifier = GetNumber(TheAbility.Modifier, "Modifikator", true);
		}

		public static Ability SelectAbility(AbilitiesRepository abilities)
		{
			Console.WriteLine("Gespeicherte Fähigkeiten");
			for (int i = 0; i < abilities.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {abilities.SortedItems[i].Name} ({abilities.SortedItems[i].Id})");
			}
			var selectedAbility = GetNumber(0, "Wähle eine Fähigkeit", false);
			return (Ability)abilities.SortedItems[selectedAbility];
		}
	}
}