using System;
using System.Collections.Generic;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.MagicAndSpells;
using DungeonsAndDragons.Repositories;

namespace DungeonsAndDragons.Interviews
{

    public class SpellInterview : Interview {

        public Spell TheSpell {get; set;}
        public ClassRepository Classes  {get; set;}
        public SpellInterview(ClassRepository classes, Spell theSpell)
        {
            Classes = classes;
            if(theSpell!=null)
            {
                TheSpell = theSpell;
            }else{
                TheSpell = new Spell();
                TheSpell.CastingTime = 1;
                TheSpell.Range = 60;
            }
        }

		public SpellInterview()
		{
		}

		public void RunInterview(){
            PrintHeader("Charakter", "Zaubersprüche", "Füge neue Zaubersprüche der Sammlung hinzu");
            TheSpell.AllowedClasses = GetAllowedClasses();
            TheSpell.Name = GetName(TheSpell.Name);
            TheSpell.CastingTime = GetNumber(TheSpell.CastingTime, $"Wie viel Zeit kostest es den Zauber zu wirken? [{TheSpell.CastingTime} Aktion(en)], false", true);
            TheSpell.RangeType = GetRangeType(TheSpell.RangeType);
            TheSpell.Range = GetNumber(TheSpell.Range, $"Wie weit wirkt der Zauber? [{TheSpell.Range} ft]", true);
            TheSpell.Components = GetComponents(TheSpell.Components);
            TheSpell.Duration = GetNumber(TheSpell.Duration, $"Wie lang wirkt der Zauber [{TheSpell.Duration} Stunden]", false);
            TheSpell.Summary = GetLine(TheSpell.Summary, "Welche Wirkung hat der Zauberspruch", "");
        }

        private List<Archetypes> GetAllowedClasses()
        {
            var ret = new List<Archetypes>(); 
            Console.WriteLine($"Verfügbare Charakter Klassen" );
            for (int i = 1; i <= Classes.Items.Count; i++)
            {
                Console.WriteLine($"{i}. {Classes.Items[i-1].Name}");
            }

            var numbers = GetNumbers("Klasse(n) wählen");

            //foreach (var num in numbers)
            //{
            //    ret.Add(((CharacterClass)Classes.Items[num-1]).ClassType);
            //}
            return ret;
        }

		internal static Spell SelectSpell(SpellsRepository spells)
		{
			Console.WriteLine("Gespeicherte Zaubersprüche");
			for (int i = 0; i < spells.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {spells.SortedItems[i].Name} ({spells.SortedItems[i].Id})");
			}
			var selectedSpell = GetNumber(0, "Wähle einen Zauberspruch", false);
			return (Spell)spells.SortedItems[selectedSpell];
		}

		private RangeType GetRangeType(RangeType rangeType)
        {
            var comps = new List<Component>();
            Console.WriteLine("Mögliche Entfernung");
            Console.WriteLine("1. Berührung (nah)");
            Console.WriteLine("2. Distanz");
           
            var selecteNumber = GetNumber((int)rangeType, "Wähle Entfernungstyp", false);
            if(selecteNumber==1)
                return RangeType.Touch;
            if(selecteNumber==2)
                return RangeType.Distance;

            return RangeType.Touch;
        }

      
        private List<Component> GetComponents(List<Component> components)
        {
            var comps = new List<Component>();
            Console.WriteLine("Komponenten des Zaubers");
            Console.WriteLine("1. Sprache (V)");
            Console.WriteLine("2. Sprache & Bewegung (V, S)");
            Console.WriteLine("3. Sprache, Bewegung und zusätzliche Bestandteile (V, S, M)");

            var selectedNumber = GetNumber(0, $"Wähle V oder V,S oder V,S,M:", false);

            if(selectedNumber==3){
                comps.Add(new Component {ComponentType=ComponentTypes.Somatic, Name="Bewegung"});
                Console.Write("Welche Bestandteile werden benötigt: ");
                var mAnswer = Console.ReadLine();
                comps.Add(new Component {ComponentType=ComponentTypes.Material, Name="Zusätzliche Bestandteile", Material=mAnswer});
            } 
            if(selectedNumber==2){
                comps.Add(new Component {ComponentType=ComponentTypes.Verbal, Name="Sprache"});
            }
            comps.Add(new Component {ComponentType=ComponentTypes.Verbal, Name="Sprache"});
            return comps;
        }

    }
}