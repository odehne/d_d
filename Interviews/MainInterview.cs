using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Tools;
using System;

namespace DungeonsAndDragons.Interviews
{
	public class MainInterview : Interview
    {
        public static void RunInterview(){
			Program.Reload();
            Menu();
        }

        public static void Menu() {
			do
			{

				PrintHeader("Hauptmenü", "Character, Monster, Ethnien, Waffen, usw.", "Wählen was Du machen willst");

				Console.WriteLine();
				Console.WriteLine("1. Waffe hinzufügen o. ändern");
				Console.WriteLine("2. Rüstung hinzufügen o. ändern");
				Console.WriteLine("3. Zauberspruch hinzufügen o. ändern");
				Console.WriteLine("4. Ethinie hinzufügen o. ändern");
				Console.WriteLine("5. Klasse hinzufügen o. ändern");
				Console.WriteLine("6. Monster hinzufügen o. ändern");
				Console.WriteLine("7. Spieler Character hinzufügen o. ändern");
				Console.WriteLine("8. Fähigkeiten hinzufügen o. ändern");
				Console.WriteLine("9. Schadensklassen ändern");
				Console.WriteLine("10. Größeneinheiten ändern");
				Console.WriteLine("11. Werkzeuge/Musikinstrumente hinzufügen o. ändern");
				Console.WriteLine("12. Rucksäcke ändern o. hinzufügen");
				Console.WriteLine("13. Rucksackgegenstände ändern o. hinzufügen");
				Console.WriteLine("14. Waffeneigenschaftem ändern o. hinzufügen");
				Console.WriteLine("--------------------------------------------");
				Console.WriteLine("15. Waffen anzeigen");
				Console.WriteLine("16. Rüstungen anzeigen");
				Console.WriteLine("17. Rucksäcke anzeigen");
				Console.WriteLine("18. Werkzeuge/Instrumente anzeigen");
				Console.WriteLine("19. Beenden");
				Console.WriteLine();
				var selectedNumber = GetNumber(1, $"Menü wählen", true);

				switch (selectedNumber)
				{
					case 1:
						var intiWepon = new WeaponInterview();
						if (YesNo("Vorhandene Waffe laden"))
						{
							var wp = WeaponInterview.SelectWeapon(Program.Weapons);
							intiWepon = new WeaponInterview(wp);
						}
						intiWepon.RunInterview();
						intiWepon.TheWeapon.Save();
						Program.LoadWeapons();
						break;
					case 2:
						var intiArmor = new ArmorInterview();
						if (YesNo("Vorhandene Rüstung laden"))
						{
							var wp = ArmorInterview.SelectArmor(Program.Armor);
							intiArmor = new ArmorInterview(wp);
						};
						intiArmor.RunInterview();
						intiArmor.TheArmor.Save();
						break;
					case 3:
						var intiSpell = new SpellInterview();
						if (YesNo("Vorhandenen Zauberspruch laden"))
						{
							var wp = SpellInterview.SelectSpell(Program.Spells);
							intiSpell= new SpellInterview(Program.Classes, wp);
						};
						intiSpell.RunInterview();
						intiSpell.TheSpell.Save();
						break;
					case 4:
						var intiRace = new RaceInterview();
						if (YesNo("Vorhandenen Ethnie laden"))
						{
							var wp = RaceInterview.SelectRace(Program.Races);
							intiRace = new RaceInterview(wp);
						};
						intiRace.RunInterview();
						intiRace.TheRace.Save();
						break;
					case 5:
						var intiClass = new CharacterClassInterview();
						if (YesNo("Vorhandenen Klasse laden"))
						{
							var wp = CharacterClassInterview.SelectClass(Program.Classes);
							intiClass = new CharacterClassInterview(wp);
						};
						intiClass.RunInterview();
						intiClass.TheClass.Save();
						break;
					case 8:
						var initAbility = new AbilityInterview();
						if (YesNo("Vorhandene Fähigkeit laden"))
						{
							var abi = AbilityInterview.SelectAbility(Program.Abilities);
							initAbility = new AbilityInterview(abi);
						};
						initAbility.RunInterview();
						initAbility.TheAbility.Save();
						Program.LoadAbilities();
						break;
					case 9:
						var intiDamageTypes = new DamageTypeInterview();
						if (YesNo("Vorhandene Schadensklasse laden"))
						{
							var abi = DamageTypeInterview.SelectDamageType(Program.DamageTypes);
							intiDamageTypes = new DamageTypeInterview(abi);
						};
						intiDamageTypes.RunInterview();
						intiDamageTypes.TheDamageType.Save();
						Program.LoadAbilities();
						break;
					case 10:
						var intiSizes = new SizeInterview();
						if (YesNo("Vorhandene Größeneinheiten laden"))
						{
							var abi = SizeInterview.SelectSizes(Program.Sizes);
							intiSizes = new SizeInterview(abi);
						};
						intiSizes.RunInterview();
						intiSizes.TheSize.Save();
						Program.LoadAbilities();
						break;
					case 11:
						var intiTools = new ToolsInterview();
						if (YesNo("Vorhandene Werkzeuge/Musikinstrumente laden"))
						{
							var abi = ToolsInterview.SelectTool(Program.Tools);
							if (abi == null) abi = new Tool();
							intiTools = new ToolsInterview(abi);
						};
						intiTools.RunInterview();
						intiTools.TheTool.Save();
						break;
					case 12:
						var intiBackpack = new BackpackInterview();
						if (YesNo("Vorhandene Rucksack laden"))
						{
							var abi = BackpackInterview.SelectPack(Program.Packs);
							if (abi == null) abi = new Pack();
							intiBackpack = new BackpackInterview(abi);
						};
						intiBackpack.RunInterview();
						intiBackpack.ThePack.Save();
						break;
					case 13:
						var intiBackpackItem = new BackpackItemInterview();
						if (YesNo("Vorhandene Gegenstände laden"))
						{
							var abi = BackpackItemInterview.SelectBackpackItem(Program.BackpackItems);
							if (abi == null) abi = new BackpackItem();
							intiBackpackItem = new BackpackItemInterview(abi);
						};
						intiBackpackItem.RunInterview();
						intiBackpackItem.TheItem.Save();
						break;
					case 14:
						var intiProps = new PropertyInterview();
						if (YesNo("Vorhandene Waffeneigenschaften laden"))
						{
							var abi = PropertyInterview.SelectProperty(Program.Properties);
							if (abi == null) abi = new Property();
							intiProps = new PropertyInterview(abi);
						};
						intiProps.RunInterview();
						intiProps.TheProperty.Save();
						break;
					case 15:
						foreach (Weapon weap in Program.Weapons.SortedItems)
						{
							Console.WriteLine(weap.ToString());
						}
						YesNo("Zurück zum Hauptmenu");
						break;
					case 16:
						foreach (Armor arm in Program.Armor.SortedItems)
						{
							Console.WriteLine(arm.ToString());
						}
						YesNo("Zurück zum Hauptmenu");
						break;
					case 17:
						foreach (Pack pack in Program.Packs.SortedItems)
						{
							Console.WriteLine(pack.ToString());
						}
						YesNo("Zurück zum Hauptmenu");
						break;
					case 18:
						foreach (ToolClass tc in Program.ToolClasses.SortedItems)
						{
							Console.WriteLine(tc.ToString(Program.Tools));
						}
						YesNo("Zurück zum Hauptmenu");
						break;
					case 19:
						Environment.Exit(0);
						break;
				}


			} while (true);
           

		}

    } 
}