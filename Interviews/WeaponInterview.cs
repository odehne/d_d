using System;
using System.Collections.Generic;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Repositories;

namespace DungeonsAndDragons.Interviews
{

	//https://dnd5e.info/equipment/weapons/

	public class WeaponInterview : Interview 
    {
        public Weapon TheWeapon { get; set; }

        public WeaponInterview()
        {
            TheWeapon = new Weapon();
        }

        public WeaponInterview(Weapon weapon)
        {
            TheWeapon = weapon;
        }

         public void RunInterview(){
            PrintHeader("Charakter", "Waffen", "Füge eine neue Waffe der Sammlung hinzu");
            TheWeapon.Category = Categories.Items;
            TheWeapon.Id = GetLine(TheWeapon.Id, "Name", "(engl.)");
            TheWeapon.Name = GetLine(TheWeapon.Name, "Name", "(deutsch)");
			TheWeapon.Class = GetClass(TheWeapon.Class);
			TheWeapon.WeaponProperties = GetProperties(TheWeapon.WeaponProperties, TheWeapon); 
            TheWeapon.Description = GetLine(TheWeapon.Description, "Beschreibung", "(in Klammern)"); 
            TheWeapon.Rarity = GetRarity();
            TheWeapon.Reach = GetNumber(TheWeapon.Reach, "Wirkungsbereich beim Nahkampf (5 ft)", true);
            TheWeapon.Weight = GetDouble(TheWeapon.Weight, "Wie schwer ist die Waffe (2.0 lb)");
            TheWeapon.AttackBonus = GetNumber(TheWeapon.AttackBonus, "Angriffsbonus", true);
            TheWeapon.DamageDie = GetDamageDie(TheWeapon.DamageDie);
			TheWeapon.DamageType = GetDamageType(TheWeapon.DamageType);
			TheWeapon.Cost = GetCost(TheWeapon.Cost);
        }

		private WeaponClass GetClass(WeaponClass properties)
		{
			Console.WriteLine("Waffenklassen");
			for (int i = 0; i < Program.WeaponClasses.Items.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.WeaponClasses.Items[i].Name}({Program.WeaponClasses.Items[i].Id})");
			}
			var selectedProp = GetNumber(0, "Klasse wählen", false);
			return (WeaponClass)Program.WeaponClasses.Items[selectedProp];
		}


		private Cost GetCost(Cost cost)
		{
			cost.Value = GetDouble(cost.Value, "Wie viel kostet die Waffe");
			Console.WriteLine("Währungseinheiten");
			for (int i = 0; i < Program.CurrencyUnits.Items.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.CurrencyUnits.Items[i].Name}({Program.CurrencyUnits.Items[i].Id})");
			}
			var selected = GetNumber(0, "Währungseinheit wählen", false);
			cost.Unit = (CurrencyUnit)Program.CurrencyUnits.Items[selected];
			return cost;
		}

		private List<Property> GetProperties(List<Property> properties, Weapon wp)
		{
			var props = new List<Property>();
			do
			{
				if (!YesNo("Eine weitere Eigenschaft der Waffe hinzufügen"))
					return props;
				for (int i = 0; i < Program.Properties.Items.Count; i++)
				{
					Console.WriteLine($"{i+1}. {Program.Properties.Items[i].Name}({Program.Properties.Items[i].Id})");
				}
				var selectedProp = GetNumber(0, "Eigenschaft wählen", false);
				var prop = (Property)Program.Properties.Items[selectedProp];
				if (prop.Range)
				{
					wp.Range.Normal = GetNumber(wp.Range.Normal, "Normale Entfernung (ft)", true);
					wp.Range.Maximum= GetNumber(wp.Range.Maximum, "Maximale Entfernung (ft, Angriffsnachteil)", true);
				}
				if (prop.DamageDie)
				{
					wp.AdditionalDamageDie = GetDamageDie(wp.AdditionalDamageDie);
				}
				props.Add(prop);
			} while (true) ;
		}

		internal static Weapon SelectWeapon(WeaponRepository weapons)
		{
			Console.WriteLine("Gespeicherte Waffen");
			for (int i = 0; i < weapons.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {weapons.SortedItems[i].Name} ({weapons.SortedItems[i].Id})");
			}
			var selectedWeapon = GetNumber(0, "Wähle eine Waffe", false);
			return (Weapon)weapons.SortedItems[selectedWeapon];
		}

		private Damage GetDamageDie(Damage die)
        {
	        var damageDie = new Damage();
			var d = new Die();
			var selected = 0;
			var i = 1;

            foreach (string dt in Enum.GetNames(typeof(DiceType))){
                Console.WriteLine($"{i}. {dt}");
				if (die != null)
					if (die.AttackDie.ToString() == dt)
						selected = i;
				i++;
            }
            var typ = GetNumber(selected,"Angriffswürfel wählen", false);    
            d.Type = (DiceType)typ;
			damageDie.AttackDie = d;

			if (d.Type == DiceType.d6)
			{
				damageDie.RollTwice = YesNo("Soll der Würfel zwei mal gerollt werden (Greatsword 2d6)");
			}
			return damageDie;
		}

        private DamageType GetDamageType(DamageType dt)
        {
			var selected = 0;
			var lst = new List<DamageType>();
			for (int i = 0; i < Program.DamageTypes.Items.Count; i++)
			{
				if (dt != null)
					if (dt.Id == Program.DamageTypes.Items[i].Id)
						selected = i;
				Console.WriteLine($"{i+1}. {Program.DamageTypes.Items[i].Name}");
			}
            var selectedNumber = GetNumber(selected, $"Wie wirkt sich der Schaden aus", false);
            return (DamageType)Program.DamageTypes.Items[selectedNumber];
        }

        private Rarity GetRarity()
        {
            return Program.Rarities.GetRarity("Common");
        }
    }
}