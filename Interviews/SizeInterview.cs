﻿using DungeonsAndDragons.Ethnicities.Sizes;
using DungeonsAndDragons.Repositories;
using System;

namespace DungeonsAndDragons.Interviews
{

	public class SizeInterview : Interview
	{
		public Size TheSize { get; set;}

		public SizeInterview(Size theSize)
		{
			TheSize = theSize;
		}

		public SizeInterview()
		{
			TheSize = new Size();
		}

		public void RunInterview()
		{
			TheSize.Id = GetLine(TheSize.Id, "Name der Größeneinheit", "(engl.)");
			TheSize.Name = GetLine(TheSize.Name, "Name der Größeneinheit", "(deutsch)");
			TheSize.Height = GetNumber(TheSize.Height, "Höhe (ft)", true);
			var spaceX  = GetDouble(TheSize.Space.Item1, "Raum (Tiefe X in ft)");
			var spaceY = GetDouble(TheSize.Space.Item2, "Raum (Tiefe Y in ft)");
			TheSize.Space = new Tuple<double, double>(spaceX, spaceY);
			TheSize.Examples = GetLine(TheSize.Examples, "Größen-Beispiele", "");
		}

		public static Size SelectSizes(SizeRepository sizes)
		{
			Console.WriteLine("Gespeicherte Größen");
			for (int i = 0; i < sizes.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {sizes.SortedItems[i].Name} ({sizes.SortedItems[i].Id})");
			}
			var selectedSize = GetNumber(0, "Wähle eine Größe", false);
			return (Size)sizes.SortedItems[selectedSize];
		}
	}
}