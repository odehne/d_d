using System;
using System.Collections.Generic;
using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Ethnicities;
using DungeonsAndDragons.Ethnicities.Sizes;
using DungeonsAndDragons.Languages;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Tools;
using DungeonsAndDragons.Traits;

namespace DungeonsAndDragons.Interviews
{
    public class RaceInterview : Interview {
        public Race TheRace { get; set; }
		public RaceInterview(Race theRace)
		{
			TheRace = theRace;
		}

		public RaceInterview()
        {
            TheRace = new Race();
        }

        public void RunInterview() {
            PrintHeader("Charakter Erschaffung","Ethnische Abstammung", "Lege die ethnische Zugehörigkeit fest");
            var et = GetEthnicity();
            TheRace.Name = et.Item1;
            TheRace.EthnicityType = et.Item2;
            PrintHeader(TheRace.Name,"Details", "Nenne ein paar Details zum Hintergrund");
            TheRace.Details = GetDetails();
            PrintHeader(TheRace.Name,"Größe und Statur", "Gib ein paar Informationen zur Statur und Größe");
            var size = GetSize();
            PrintHeader(TheRace.Name,"Geschwindigkeit", "Geschwindingkeit und Beweglichkeit");
            var speed = GetSpeed(TheRace.Attributes.Speed);
            TheRace.Attributes = new Attributes(size, speed);
            PrintHeader(TheRace.Name,"Merkmale und Schwächen", "Bestimme Stärken und Schwächen");
            TheRace.Traits = GetTraits();
            PrintHeader(TheRace.Name,"Mentale Ausrichtung", "Ist der Charakter eher rechtschaffend oder hinterhältig und böse");
            TheRace.Alignment = GetAlignment(TheRace.Alignment);
            PrintHeader(TheRace.Name,"Fähigkeiten Boni", "Lege fest welche Fähigkeiten besondere Boni bekommen");
            TheRace.AbilityScoreIncreases = GetAbilityScoreIncreases();
            PrintHeader(TheRace.Name,"Sprachen", "Welche Sprachen spricht ein Charakter dieses Volkes");
            TheRace.CanSpeak = GetLanguages();
            PrintHeader(TheRace.Name,"Zaubersprüche", "Welche Zauber beherrscht diese Ethnie");
            TheRace.AllowedCantrips = GetAllowedCantrips(TheRace.AllowedCantrips);
            if(TheRace.AllowedCantrips>0){
                PrintHeader(TheRace.Name,"Zaubersprüche", "Frei wählbare Zaubersprüche mancher Völker");
                TheRace.AllowedCantripClass = GetAllowedCantripClass();
            }
            PrintHeader(TheRace.Name,"Geschick mit Waffen", "Besonderes Waffentraining und ähnliches");
            TheRace.WeaponProficiencies = GetWeaponProficiencies();
            PrintHeader(TheRace.Name,"Geschick mit Rüstungen", "Besonderes Rüstungstraining und ähnliches");
            TheRace.ArmorProficiencies = GetArmorProficiencies();
            PrintHeader(TheRace.Name,"Geschick mit Werkzeugen", "Von Schmiedekunst bis Schlösserknacken");
            TheRace.ToolProficiencies = GetToolsProficiencies();
        }

		internal static Race SelectRace(RaceRepository races)
		{
			Console.WriteLine("Gespeicherte Klassen");
			for (int i = 0; i < races.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {races.SortedItems[i].Name} ({races.SortedItems[i].Id})");
			}
			var selectedRace = GetNumber(0, "Wähle eine Ethnie", false);
			return (Race)races.SortedItems[selectedRace];
		}

		private ArmorProficiencies GetArmorProficiencies()
        {
            var wps = new ArmorProficiencies();
            if(!YesNo("Gibt es ein Rüstungstraining (Armor proficiency)"))
                return null;
            var proTrainingsName = GetLine("","Name des Trainingsprogramms", "");
            wps.Name = proTrainingsName;

            do
            {
                if(!YesNo("Eine bestimmte Rüstung hinzufügen"))
                    return wps;
                
                var i = 1;
                foreach (var weap in Program.Armor.Items){
                    Console.WriteLine($"{i}. {weap}");
                    i++;
                }
                var wt = GetNumber(0, "Rüstung wählen", false);
                wps.Add(new ArmorProficiency(Program.Armor.Items[wt]));

            }while(true);
        }

        private WeaponProficiencies GetWeaponProficiencies()
        {
            var wps = new WeaponProficiencies();
            
            if(!YesNo("Gibt es ein Waffentraining (Weapon proficiency)"))
                return null;
            var proTrainingsName = GetLine("","Name des Trainingsprogramms", "");
            wps.Name = proTrainingsName;

            do
            {
                if(!YesNo("Eine bestimmte Waffe hinzufügen"))
                 return wps;
                
                var i = 1;
                foreach (var weap in Program.Weapons.Items){
                    Console.WriteLine($"{i}. {weap}");
                    i++;
                }
                var wt = GetNumber(0,"Waffe wählen", false);
                wps.Add(new WeaponProficiency(Program.Weapons.Items[wt]));

            }while(true);
        }

        private ToolProficiencies GetToolsProficiencies()
        {
            var wps = new ToolProficiencies();
           
            if(!YesNo("Gibt es ein Werkzeugtraining (Tools proficiency)"))
                return null;
            var proTrainingsName = GetLine("","Name des Trainingsprogramms", "");
            wps.Name = proTrainingsName;

            do
            {
                if(!YesNo("Ein bestimmtes Werkzeug hinzufügen"))
                    return wps;
                
                var i = 1;
                foreach (var weap in Program.Tools.Items){
                    Console.WriteLine($"{i}. {weap}");
                    i++;
                }
                var wt = GetNumber(0, "Werkzeug wählen", false);
                wps.Add(new ToolProficiency(Program.Tools.Items[wt]));

            }while(true);
        }

        private Archetypes GetAllowedCantripClass()
        {
            do
            {
                Console.WriteLine("Zauberspruch Klassen");
                var ft = new AbilityScoreIncrease();
                Console.WriteLine($"1. Barde - {Archetypes.Bard.ToString()}");
                Console.WriteLine($"2. Kleriker - {Archetypes.Cleric.ToString()}");
                Console.WriteLine($"3. Druide - {Archetypes.Druid.ToString()}");
                Console.WriteLine($"4. Monk - {Archetypes.Monk.ToString()}");
                Console.WriteLine($"5. Zauberer - {Archetypes.Sorcerer.ToString()}");
                Console.WriteLine($"6. Magier - {Archetypes.Wizard.ToString()}");
                Console.WriteLine($"7. Hexenmeister - {Archetypes.Warlock.ToString()}");
                
                var selectedNumber = GetNumber(0, "Aus welcher Klasse kann der Zauberspruch gewählt werden", false);
                return (Archetypes)selectedNumber-1;
            }while(true);
        }

       

        private int GetAllowedCantrips(int allowedCantrips)
        {
            return GetNumber(allowedCantrips,"Wie viele Zaubersprüche stehen zur Verfügung", true);
        }

        private CanSpeak GetLanguages()
        {
            var langs = new Languages.CanSpeak();
            langs.Add(new Language("Common", "Allgemein"));
            do
            {
                if(!YesNo("Noch eine Sprache (außer Common) hinzufügen"))
                    return langs;

                var ln = GetLine("", "Sprachname", "(engl.)");
                var l = GetLine("", "Sprachname", "(deutsch.)");
              
                langs.Add(new Language(ln, l));
            }while(true);

        }

        private List<AbilityScoreIncrease> GetAbilityScoreIncreases()
        {
            var asi = new List<AbilityScoreIncrease>();

            do
            {
                if(!YesNo("Fähigkeiten Boni hinzufügen"))
                    return asi;
                var ft = new AbilityScoreIncrease();
                Console.WriteLine();
                Console.WriteLine($"1. Stärke - {AbilityType.Strength.ToString()}");
                Console.WriteLine($"2. Geschicklichkeit - {AbilityType.Dexterity.ToString()}");
                Console.WriteLine($"3. Konstitution - {AbilityType.Constitution.ToString()}");
                Console.WriteLine($"4. Intelligenz - {AbilityType.Intelligence.ToString()}");
                Console.WriteLine($"5. Weisheit - {AbilityType.Wisdom.ToString()}");
                Console.WriteLine($"6. Charisma - {AbilityType.Charisma.ToString()}");
                Console.WriteLine();
                var selectedNumber = GetNumber(0, $"Fähigkeit wählen", false);
                ft.AbilityType = (AbilityType)selectedNumber-1;
                var bonus = GetNumber(0, $"Bonus", true);
                ft.Bonus = bonus;
                asi.Add(ft);
            }while(true);
        }

        private Alignment GetAlignment(Alignment org)
        {
            var ret = new Alignment();
            ret.Name = "Ausrichtung";
            ret.Description = GetLine(org.Description, "Beschreibe typische Charaktermerkmale","");
            ret.Example =  GetLine(org.Example, "Beispiele","");
            return ret;
        }

        private List<Trait> GetTraits()
        {
            var fts = new List<Trait>();

            do
            {
                if(!YesNo("Merkmale (Features, Traits) hinzufügen")) 
                    return fts;
                var ft = new Traits.Trait();
                ft.Id = GetLine("","FeatureName","(engl.)" );
                ft.Name = GetLine("","FeatNameureName","(deutsch)" );
                ft.Description =  GetLine("","Beschreibung, Wirkung","" );
                var isTrait = GetLine("","Eher nachteilig","(y|N)" );
                if(isTrait.Equals("y", StringComparison.CurrentCultureIgnoreCase))
                    ft.IsBad = true;

            }while(true);
        }

        private int GetSpeed(int org)
        {
            var speed = GetNumber(org,$"Geschwindigkeit festlegen (30ft)", true);
            return speed;
        }

        private Size GetSize()
        {
            var ret = new Size();
            Console.WriteLine();
            Console.WriteLine($"1. Winzig - {SizeCategories.Tiny.ToString()}");
            Console.WriteLine($"2. Klein - {SizeCategories.Small.ToString()}");
            Console.WriteLine($"3. Normal - {SizeCategories.Medium.ToString()}");
            Console.WriteLine($"4. Groß - {SizeCategories.Large.ToString()}");
            Console.WriteLine($"5. Riesig - {SizeCategories.Huge.ToString()}");
            Console.WriteLine($"6. Gigantisch - {SizeCategories.Gargantuan.ToString()}");
            Console.WriteLine();
            var selectedNumber = GetNumber(0, $"Größe wählen", false);
            switch (selectedNumber) {
                case 1:
                    ret.Id = "tiny";
                    ret.Name = "Winzig";
                    ret.Space = new Tuple<double, double>(2.5, 2.5);
                    ret.Height = 1;
                    break;
                case 2:
                    ret.Id = "small";
					ret.Name = "Klein";
                    ret.Space = new Tuple<double, double>(5, 5);
                    ret.Height = 2;
                    break;
                case 3:
                    ret.Id= "medium";
                    ret.Name = "Normal";
                    ret.Space = new Tuple<double, double>(5, 5);
                    ret.Height = 6;
                    break;
                case 4:
                    ret.Id = "large";
                    ret.Name = "Groß";
                    ret.Space = new Tuple<double, double>(10, 10);
                    ret.Height = 13;
                    break;
                case 5:
                    ret.Id = "huge2";
                    ret.Name = "Riesig";
                    ret.Space = new Tuple<double, double>(15, 15);
                    ret.Height = 25;
                    break;
                case 6:
                    ret.Id = "gargantuan";
                    ret.Name = "Gigantisch";
                    ret.Space = new Tuple<double, double>(20, 20);
                    ret.Height = 100;
                    break;
            }
            
            return ret;
        }

        private string GetDetails() {
            Console.Write($"Nenne ein paar details: " );
            var details = Console.ReadLine();
            return details;
        }



        private Tuple<string, EthnicityTypes> GetEthnicity()
        {
            Console.WriteLine($"1. Elben - ({EthnicityTypes.Elf.ToString()})");
            Console.WriteLine($"2. Elben - Düsterelben ({EthnicityTypes.DarkElf.ToString()})");
            Console.WriteLine($"3. Elben - Hochelben ({EthnicityTypes.HighElf.ToString()})");
            Console.WriteLine($"4. Elben - Waldelben ({EthnicityTypes.WoodElf.ToString()})");
            Console.WriteLine($"5. Halbelben - ({EthnicityTypes.HalfElf.ToString()})");
            Console.WriteLine($"6. Menschen - ({EthnicityTypes.Human.ToString()})");
            Console.WriteLine($"7. Hobbits - ({EthnicityTypes.Halfling.ToString()})");
            Console.WriteLine($"8. Hobbits - Stout ({EthnicityTypes.Stout.ToString()})");
            Console.WriteLine($"9. Hobbits - Leichtfuß ({EthnicityTypes.LightFoot.ToString()})");
            Console.WriteLine($"10. Zwerge - ({EthnicityTypes.Dwarf.ToString()})");
            Console.WriteLine($"11. Zwerge - Hügelzwerge ({EthnicityTypes.HillDwarf.ToString()})");
            Console.WriteLine($"12. Zwerge - Gebirgszwerge ({EthnicityTypes.MountainDwarf.ToString()})");
            Console.WriteLine($"13. Gnome - ({EthnicityTypes.Gnome.ToString()})");
            Console.WriteLine($"14. Gnome - Waldgnome ({EthnicityTypes.ForestGnome.ToString()})");
            Console.WriteLine($"15. Gnome - Felsgnome ({EthnicityTypes.ForestGnome.ToString()})");
            Console.WriteLine($"16. Halbork - ({EthnicityTypes.HalfOrk.ToString()})");
            Console.WriteLine($"17. Tieflings - ({EthnicityTypes.Tiefling.ToString()})");
            Console.WriteLine($"18. Drachengeborene - ({EthnicityTypes.DragonBorn.ToString()}))");
            Console.WriteLine();
            var selectedNumber = GetNumber(0, $"Ethnie wählen [{TheRace.Name}]", true);
            return GetRace(selectedNumber);
           
        }

        private Tuple<string, EthnicityTypes> GetRace(int v)
        {
            switch (v)
            {
                case 1:
                    return new Tuple<string, EthnicityTypes>("Elb", EthnicityTypes.Elf);
                case 2: 
                    return new Tuple<string, EthnicityTypes>("Elb (Düsterelb)", EthnicityTypes.DarkElf);
                case 3: 
                    return new Tuple<string, EthnicityTypes>("Elb (Hochelb)", EthnicityTypes.HighElf);
                case 4: 
                    return new Tuple<string, EthnicityTypes>("Elb (Waldelb)", EthnicityTypes.WoodElf);
                case 5: 
                    return new Tuple<string, EthnicityTypes>("Halbelb",  EthnicityTypes.HalfElf);
                case 6: 
                    return new Tuple<string, EthnicityTypes>("Mensch",  EthnicityTypes.Human);
                case 7: 
                    return new Tuple<string, EthnicityTypes>("Hobbit",  EthnicityTypes.Halfling);
                case 8: 
                    return new Tuple<string, EthnicityTypes>("Hobbit (Stout)",  EthnicityTypes.Stout);
                case 9: 
                    return new Tuple<string, EthnicityTypes>("Hobbit (Leichtfuß)",  EthnicityTypes.LightFoot);
                case 10: 
                    return new Tuple<string, EthnicityTypes>("Zwerg",  EthnicityTypes.Dwarf);
                case 11: 
                    return new Tuple<string, EthnicityTypes>("Zwerg (Hügelzwerg)",  EthnicityTypes.HillDwarf);
                case 12: 
                    return new Tuple<string, EthnicityTypes>("Zwerg (Gebirgszwerg)",  EthnicityTypes.MountainDwarf);
                case 13: 
                    return new Tuple<string, EthnicityTypes>("Gnom",  EthnicityTypes.Gnome);
                case 14: 
                    return new Tuple<string, EthnicityTypes>("Gnom (Waldgnome)",  EthnicityTypes.ForestGnome);
                case 15: 
                    return new Tuple<string, EthnicityTypes>("Gnom (Felsgnom)",  EthnicityTypes.RockGnome);
                case 16: 
                    return new Tuple<string, EthnicityTypes>("Halbork",  EthnicityTypes.HalfOrk);
                case 17: 
                    return new Tuple<string, EthnicityTypes>("Tiefling",  EthnicityTypes.Tiefling);
                case 18: 
                    return new Tuple<string, EthnicityTypes>("Drachengeborener",  EthnicityTypes.DragonBorn);
            }
             return new Tuple<string, EthnicityTypes>("Unbekannt", EthnicityTypes.Unborn);
        }
    }
}