using System;
using System.Collections.Generic;
using System.Text;

namespace DungeonsAndDragons.Interviews
{

    public class Interview {

        public const string HeaderStars = "*******************************************************************************";

        public static void PrintHeader(string chapter, string headline, string subHeadline){
            Console.Clear();
            Console.WriteLine(HeaderStars);
            Console.WriteLine(CenterText(chapter));
            Console.WriteLine();
            Console.WriteLine(CenterText(headline));
            Console.WriteLine(CenterText(subHeadline));
            Console.WriteLine();
            Console.WriteLine(HeaderStars);
            Console.WriteLine();
       }

        public static string CenterText(string txt){
            var fullLen = HeaderStars.Length;
            var len = txt.Length;
            if(len>=fullLen)
                return txt;
            var c = (fullLen-len)/2;
            var spcs = ""; 
            for (int i = 0; i < c; i++)
            {
                spcs+=" ";
            } 
            return spcs + txt;  
        }

        public static bool YesNo(string question) {
            var answer = GetLine("", question, "(y|N)");
		    if(answer.ToLower()=="y") 
                return true;
			if(answer.ToLower()=="m")
				MainInterview.RunInterview();
			return false;
        }

        public static string GetLine(string orgValue, string question, string help, bool longLine = false) {
			var option = "";
			var answer = "";
			if (!string.IsNullOrEmpty(orgValue))
				option = $" [{orgValue}]";
            Console.Write($"{question} {help}{option}: ");

			answer = (!longLine) ? Console.ReadLine() : GetLongLine();

			if (string.IsNullOrEmpty(answer))
                return orgValue;
			if (answer.ToLower() == "m")
				MainInterview.RunInterview();
			return answer; 
        }

		public static string GetLongLine()
		{
			var bufSize = 2046;
			var inputStream = Console.OpenStandardInput(bufSize);
			byte[] bytes = new byte[bufSize];
			int outputLength = inputStream.Read(bytes, 0, bufSize);
			char[] chars = Encoding.UTF7.GetChars(bytes, 0, outputLength);
			return new string(chars);
		}

		/// <summary>
		/// If startAtZero equals false the result is getting decrimented by 1
		/// </summary>
		/// <param name="orgValue"></param>
		/// <param name="question"></param>
		/// <param name="startsAtZero"></param>
		/// <returns></returns>
		public static int GetNumber(int orgValue, string question, bool startsAtZero) {
            do
            {
                Console.Write($"{question} [{orgValue}]: " );
                var answer = Console.ReadLine();
				if (answer.ToLower() == "m")
					MainInterview.RunInterview();
				if (Int32.TryParse(answer, out var i)){
                   if(!startsAtZero) {
                       return i-1;
                   }else{
                       return i;
                   }
                }else{
                    return orgValue;
                }
            } while (true);
        }

		/// <summary>
		/// If startAtZero equals false i will be decrimented
		/// </summary>
		/// <param name="orgValue"></param>
		/// <param name="question"></param>
		/// <param name="startsAtZero"></param>
		/// <returns></returns>
		public static double GetDouble(double orgValue, string question)
		{
			do
			{
				Console.Write($"{question} [{orgValue}]: ");
				var answer = Console.ReadLine();
				if (answer.ToLower() == "m")
					MainInterview.RunInterview();
				return double.TryParse(answer, out var i) ? i : orgValue;
			} while (true);
		}

		public static IEnumerable<int> GetNumbers(string question){
            var numbers = new List<int>();
            var isValid = false;
            do
            {
                Console.Write($"{question}: " );
                var answer = Console.ReadLine();
				if (answer.ToLower() == "m")
					MainInterview.RunInterview();
				answer = answer.Replace(" ", "");
                var s = answer.Split(",");

                foreach (var item in s)
                {
                    if(!Int32.TryParse(item, out var i)){
                        break;
                    }else{
                        isValid=true;
                        numbers.Add(i);
                    }
                }
            } while (!isValid);
            return numbers;
         }   

         public static string GetName(string name){
            do
            {
                Console.Write($"Name des Zaubers [{name}]: " );
                name = Console.ReadLine();
				if (name.ToLower() == "m")
					MainInterview.RunInterview();
				if (!string.IsNullOrEmpty(name)){
                    return name;
                }   
            } while (true);
        }
    }
}