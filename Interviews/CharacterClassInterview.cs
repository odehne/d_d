﻿using System;
using System.Collections.Generic;
using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Tools;

namespace DungeonsAndDragons.Interviews
{

	public class CharacterClassInterview : Interview
	{
		public CharacterClass TheClass { get; set; }

		public CharacterClassInterview(CharacterClass theClass)
		{
			TheClass = theClass;
		}

		public CharacterClassInterview()
		{
			TheClass = new CharacterClass();
		}

		public void RunInterview()
		{
			PrintHeader("Charakter", "Klassen", "Füge eine neue Klasse der Sammlung hinzu");

			TheClass.Id = GetLine("", "Klassenname", "(engl.)");
			TheClass.Name = GetLine("", "Klassenname", "(deutsch)");
			TheClass.HitDice = GetDie(TheClass.HitDice);
			TheClass.HitPointsFirstLevel = GetFirstLevelHitPoints(TheClass.HitPointsFirstLevel);
			TheClass.ArmorClasses = GetArmorClasses();
			TheClass.WeaponClasses = GetWeaponClasses();
			TheClass.SavingThrows = GetSaves();
			TheClass.ToolClasses = GetToolClasses();
			TheClass.HowManySkills = GetNumber(TheClass.HowManySkills, "Wie viele Skills darf der Character beherrschen", true);
			TheClass.SkillsToChooseFrom = GetSelectableSkills();
			TheClass.ProficiencyBonus = GetNumber(TheClass.ProficiencyBonus, "Fertigkeitsbonus (Proficiency Bonus)", true);
			TheClass.Features = GetFeatures();
			TheClass.Equipment = GetEquipment();
		}

		private HitPointsBasis GetFirstLevelHitPoints(HitPointsBasis hitPointsFirstLevel)
		{
			var hp = new HitPointsBasis();
			if (hitPointsFirstLevel == null)
				hitPointsFirstLevel = new HitPointsBasis();
			hp.MaxModifierValue = GetNumber(hitPointsFirstLevel.MaxModifierValue, "Maximalwert des Modifikators", true);
			hp.BaseValue = GetNumber(hitPointsFirstLevel.BaseValue, "Basiswert", true);
			hp.Ability = GetAbility();
			return hp;
		}

		private Die GetDie(Die die)
		{
			Console.WriteLine("Lebenspunkte (hit points) -Würfel");
			var d = new Die();
			var selected = 0;
			var i = 1;
			foreach (string dt in Enum.GetNames(typeof(DiceType)))
			{
				Console.WriteLine($"{i}. {dt}");
				if (die != null)
					if (die.ToString() == dt)
						selected = i;
				i++;
			}
			var typ = GetNumber(selected, "Lebenspunkte-Würfel (HP) wählen", false);
			d.Type = (DiceType)typ;
			return d;
		}

		private Equipment GetEquipment()
		{
			var eqi = new Equipment();
			Console.WriteLine();
			Console.WriteLine("Austatung (Waffen, Rüstung, Rucksack, Werkeuge)");
			Console.WriteLine("Waffengattungen");
			eqi.WeaponClassesToChoose = GetWeaponClasses();
			Console.WriteLine("Spezielle Waffen zu Auswahl");
			eqi.WeaponsToChoose = GetSelectableWeapons();

			Console.WriteLine("Rüstungsarten");
			eqi.ArmorClassesToChoose = GetArmorClasses();
			Console.WriteLine("Spezielle Rüstungen");
			eqi.ArmorToChoose = GetSelectableArmor();

			Console.WriteLine("Rucksackinhalt");
			eqi.PacksToChoose = GetPacksToChoose();

			Console.WriteLine("Werkzeugesammlungen");
			eqi.ToolClassesToChoose = GetToolClasses();

			Console.WriteLine("Werkzeuge/Spielzeug/Musikinstrumente");
			eqi.ToolToChoose = GetSelectableTools();
			return eqi;
		}

		private List<ToolClass> GetToolClasses()
		{
			var props = new List<ToolClass>();
			do
			{
				if (!YesNo("Einen weitere Werkzeugsammlung hinzufügen"))
					return props;
				for (int i = 0; i < Program.ToolClasses.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.ToolClasses.SortedItems[i].Name} ({Program.ToolClasses.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Werkzeugsammlung wählen", false);
				var prop = (ToolClass)Program.ToolClasses.SortedItems[selected];
				props.Add(prop);
			} while (true);
		}

		private List<Tool> GetSelectableTools()
		{
			var props = new List<Tool>();
			do
			{
				if (!YesNo("Einen weiteres Werkzeug hinzufügen"))
					return props;
				for (int i = 0; i < Program.Tools.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.Tools.SortedItems[i].Name} ({Program.Tools.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Werkzeug wählen", false);
				var prop = (Tool)Program.Tools.SortedItems[selected];
				props.Add(prop);
			} while (true);
		}

		private List<Feature> GetFeatures()
		{
			var fts = new List<Feature>();
			do
			{
				if (!YesNo("Ein weiteres Feature hinzufügen"))
					return fts;
				var ft = new Feature
				{
					Id = GetLine("", "Feature name", "(engl)"),
					Name = GetLine("", "Feature name", "(deutsch)"),
					Description = GetLine("", "Beschreibung", "", true)
				};
				fts.Add(ft);
			} while (true);
		}

		private List<Armor> GetSelectableArmor()
		{
			var props = new List<Armor>();
			do
			{
				if (!YesNo("Eine weitere Rüstung hinzufügen"))
					return props;
				for (int i = 0; i < Program.Armor.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.Armor.SortedItems[i].Name} ({Program.Armor.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Rüstung wählen", false);
				var prop = (Armor)Program.Armor.SortedItems[selected];
				props.Add(prop);
			} while (true);
		}

		private List<Weapon> GetSelectableWeapons()
		{
			var props = new List<Weapon>();
			do
			{
				if (!YesNo("Eine weitere Waffe hinzufügen"))
					return props;
				for (int i = 0; i < Program.Weapons.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.Weapons.SortedItems[i].Name} ({Program.Weapons.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Waffe wählen", false);
				var prop = (Weapon)Program.Weapons.SortedItems[selected];
				props.Add(prop);
			} while (true);
		}

		private List<Skill> GetSelectableSkills()
		{
			var skills = new List<Skill>();
			do
			{
				if (!YesNo("Weiteren Skill hinzufügen"))
					return skills;
				for (int i = 0; i < Program.Skills.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.Skills.SortedItems[i].Name} ({Program.Skills.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Skill wählen", false);
				var skill = (Skill)Program.Skills.SortedItems[selected];
				skills.Add(skill);
			} while (true);
		}

		private List<string> GetTools()
		{
			var tools = new List<string>();
			do
			{
				if (!YesNo("Werkzeugklassen o. Werkzeuge hinzufügen"))
					return tools;
				var selected = GetLine("", "Werkzeug", "Klasse oder Name");
				if (string.IsNullOrEmpty(selected))
					tools.Add(selected);
			} while (true);

		}

		private List<Ability> GetSaves()
		{
			var abis = new List<Ability>();
			do
			{
				if (!YesNo("Schutzeigenschaft (Saving Throw) hinzufügen"))
					return abis;
				for (int i = 0; i < Program.Abilities.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.Abilities.SortedItems[i].Name} ({Program.Abilities.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Schutzeigenschaft wählen", false);
				var abi = (Ability)Program.Abilities.SortedItems[selected];
				abis.Add(abi);
			} while (true);
		}

		private List<WeaponClass> GetWeaponClasses()
		{
			var props = new List<WeaponClass>();
			do
			{
				if (!YesNo("Eine weitere Waffengattung hinzufügen"))
					return props;
				for (int i = 0; i < Program.WeaponClasses.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.WeaponClasses.SortedItems[i].Name} ({Program.WeaponClasses.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Waffengattung wählen", false);
				var prop = (WeaponClass)Program.WeaponClasses.SortedItems[selected];
				props.Add(prop);
			} while (true);
		}

		private List<Pack> GetPacksToChoose()
		{
			var props = new List<Pack>();
			do
			{
				if (!YesNo("Einen weiteren Rucksack hinzufügen"))
					return props;
				for (int i = 0; i < Program.Packs.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.Packs.SortedItems[i].Name} ({Program.Packs.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Rucksack wählen", false);
				var prop = (Pack)Program.Packs.SortedItems[selected];
				props.Add(prop);
			} while (true);
		}

		private List<ArmorClass> GetArmorClasses()
		{
			var props = new List<ArmorClass>();
			do
			{
				if (!YesNo("Eine weitere Rüstungsklasse hinzufügen"))
					return props;
				for (int i = 0; i < Program.ArmorClasses.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.ArmorClasses.SortedItems[i].Name} ({Program.ArmorClasses.SortedItems[i].Id})");
				}
				var selected = GetNumber(0, "Rüstungsklasse wählen", false);
				var prop = (ArmorClass)Program.ArmorClasses.SortedItems[selected];
				props.Add(prop);
			} while (true);
		}


		private Ability GetAbility()
		{
			Console.WriteLine("Gespeicherte Fähigkeiten");
			for (int i = 0; i < Program.Abilities.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.Abilities.SortedItems[i].Name} ({Program.Abilities.SortedItems[i].Id})");
			}
			var selectedClass = GetNumber(0, "Wähle eine Fähigkeit als Modifikator", false);
			return (Ability)Program.Abilities.SortedItems[selectedClass];
		}

		internal static CharacterClass SelectClass(ClassRepository classes)
		{
			Console.WriteLine("Gespeicherte Klassen");
			for (int i = 0; i < classes.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {classes.SortedItems[i].Name} ({classes.SortedItems[i].Id})");
			}
			var selectedClass = GetNumber(0, "Wähle eine Klasse", false);
			return (CharacterClass)classes.SortedItems[selectedClass];
		}
	}
}