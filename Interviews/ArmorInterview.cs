using System;
using System.Collections.Generic;
using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Repositories;

namespace DungeonsAndDragons.Interviews
{
    public class ArmorInterview : Interview
    {
        public Armor TheArmor { get; set; }

        public ArmorInterview()
        {
            TheArmor = new Armor();
        }

        public ArmorInterview(Armor armor)
        {
            TheArmor = armor;
        }

        public Armor RunInterview() {
			PrintHeader("Charakter", "Rüstungen", "Füge eine neue Rüstung der Sammlung hinzu");
			TheArmor.Id = GetLine(TheArmor.Id, "Name", "(engl.)");
			TheArmor.Name = GetLine(TheArmor.Name, "Name", "(deutsch)");
			TheArmor.StealthDisadvantage = YesNo("Hat die Rüstung einen Nachteil bei Tarnung (Stealth)");
			TheArmor.Class= GetClass(TheArmor.Class);
			TheArmor.BasePoints = GetArmorDetails(TheArmor);
			TheArmor.Description = GetLine(TheArmor.Description, "Beschreibung", "(in Klammern)");
			TheArmor.Rarity = GetRarity();
			TheArmor.Weight = GetDouble(TheArmor.Weight, "Wie schwer ist die Rüstung (2.0 lb)");
			TheArmor.Cost = GetCost(TheArmor.Cost);
			return TheArmor;
        }

		private HitPointsBasis GetArmorDetails(Armor armor)
		{
			if (armor.BasePoints == null)
				armor.BasePoints = new HitPointsBasis();
			armor.BasePoints.BaseValue = GetNumber(armor.BasePoints.BaseValue, "AC Basiswert", true);
			Console.WriteLine("Rüstungsfähigkeiten");
			for (int i = 0; i < Program.Abilities.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.Abilities.SortedItems[i].Name}({Program.Abilities.SortedItems[i].Id})");
			}
			Console.WriteLine($"{Program.Abilities.SortedItems.Count+1}. Keine");
			var selected = GetNumber(0, "Rüstungsfähigkeit wählen", false);


			if (selected!= Program.Abilities.SortedItems.Count)
			{
				armor.BasePoints.Ability = (Ability)Program.Abilities.SortedItems[selected];
			}
			else
			{
				armor.BasePoints.Ability = null;
			}

			armor.BasePoints.MaxModifierValue = GetNumber(armor.BasePoints.MaxModifierValue, "Maximalwert des Modifikators", true);
			return armor.BasePoints;
		}


		private Cost GetCost(Cost cost)
		{
			if (cost == null)
				cost = new Cost();
			cost.Value = GetDouble(cost.Value, "Wie viel kostet die Rüstung");
			Console.WriteLine("Währungseinheiten");
			for (int i = 0; i < Program.CurrencyUnits.Items.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.CurrencyUnits.Items[i].Name}({Program.CurrencyUnits.Items[i].Id})");
			}
			var selected = GetNumber(0, "Währungseinheit wählen", false);
			cost.Unit = (CurrencyUnit)Program.CurrencyUnits.Items[selected];
			return cost;
		}

		private Rarity GetRarity()
		{
			return Program.Rarities.GetRarity("Common");
		}

		private ArmorClass GetClass(ArmorClass properties)
		{
			Console.WriteLine("Rüstungsklassen");
			for (int i = 0; i < Program.ArmorClasses.Items.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {Program.ArmorClasses.Items[i].Name}({Program.ArmorClasses.Items[i].Id})");
			}
			var selectedProp = GetNumber(0, "Klasse wählen", false);
			return (ArmorClass)Program.ArmorClasses.Items[selectedProp];
		}

		internal static Armor SelectArmor(ArmorRepository armor)
		{
			Console.WriteLine("Gespeicherte Rüstungen");
			for (int i = 0; i < armor.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {armor.SortedItems[i].Name} ({armor.SortedItems[i].Id})");
			}
			var selectedArmor = GetNumber(0, "Wähle eine Rüstung", false);
			return (Armor)armor.SortedItems[selectedArmor];
		}
	}
}