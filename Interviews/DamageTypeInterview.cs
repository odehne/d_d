﻿using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Repositories;
using System;

namespace DungeonsAndDragons.Interviews
{

	public class DamageTypeInterview : Interview
	{
		public DamageType TheDamageType { get; set; }

		public DamageTypeInterview(DamageType theDamageType)
		{
			TheDamageType = theDamageType;
		}

		public DamageTypeInterview()
		{
			TheDamageType = new DamageType();
		}

		public void RunInterview()
		{
			TheDamageType.Id = GetLine(TheDamageType.Id, "Name der Schadensklasse", "(engl.)");
			TheDamageType.Name = GetLine(TheDamageType.Name, "Name der Schadensklasse", "(deutsch)");
		}

		public static DamageType SelectDamageType(DamageTypeRepository damageTypes)
		{
			Console.WriteLine("Gespeicherte Schadensklassen");
			for (int i = 0; i < damageTypes.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {damageTypes.SortedItems[i].Name} ({damageTypes.SortedItems[i].Id})");
			}
			var selectedDamageType = GetNumber(0, "Wähle eine Schadensklasse", false);
			return (DamageType)damageTypes.SortedItems[selectedDamageType];
		}
	}
}