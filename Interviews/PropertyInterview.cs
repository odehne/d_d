﻿using DungeonsAndDragons.Arms;
using DungeonsAndDragons.Repositories;
using System;

namespace DungeonsAndDragons.Interviews
{
	public class PropertyInterview : Interview
	{
		public Property TheProperty { get; set; }

		public PropertyInterview(Property theProperty)
		{
			TheProperty = theProperty;
		}

		public PropertyInterview()
		{
			TheProperty = new Property();
		}

		public void RunInterview()
		{
			TheProperty.Id = GetLine(TheProperty.Id, "Name der Eigenschaft", "(engl.)");
			TheProperty.Name = GetLine(TheProperty.Name, "Name der Eigenschaft", "(deutsch)");
			TheProperty.Description = GetLine(TheProperty.Description, "Beschreibung der Eigenschaft", "");

			var addDamageDie = YesNo("Zusätzlichen Schadenswürfel hinzufügen");
			if (addDamageDie)
			{
				TheProperty.DamageDie = true;
			}
			else
			{
				TheProperty.DamageDie = false;
			}
			var addRange = YesNo("Entfernung für Fernwaffen hinzufügen");
			if (addRange)
			{
				TheProperty.Range = true;
			}
			else
			{
				TheProperty.Range = false;
			}
		}

		private DamageType GetDamageType(DamageType damageType, DamageTypeRepository damageTypes)
		{
			Console.WriteLine("Mögliche Schadensklassen");
			var i = 1;
			var selectedDt = 1;
			foreach (var dt in damageTypes.Items)
			{
				Console.WriteLine($"{i}. {dt.Name} ({dt.Id}");
				if (dt.Id.Equals(damageType.Id))
					selectedDt = i;
				i++;
			}
			var wt = GetNumber(selectedDt, "Schadensklasse wählen", false);
			return damageTypes.GetSelected(wt);
		}

		
		private Die GetAttackDie(Die attackDie)
		{
			var selectDiceType = GetAttackDieType(attackDie.Type);
			Console.WriteLine("Mögliche Angriffswürfel");
			Console.WriteLine($"1. d2");
			Console.WriteLine($"2. d4");
			Console.WriteLine($"3. d6");
			Console.WriteLine($"4. d8");
			Console.WriteLine($"5. d10");
			Console.WriteLine($"6. d20");
			var wt = GetNumber(selectDiceType, "Angeriffswürfel wählen [" + attackDie + "]", false);
			attackDie.Type = GetDiceType(wt);
			return attackDie;
		}

		private DiceType GetDiceType(int wt)
		{
			switch (wt)
			{
				case 1:
					return DiceType.d2;
				case 2:
					return DiceType.d4;
				case 3:
					return DiceType.d6;
				case 4:
					return DiceType.d8;
				case 5:
					return DiceType.d10;
				case 6:
					return DiceType.d20;
			}
			return DiceType.d2;
		}

		private int GetAttackDieType(DiceType dt)
		{
			switch (dt)
			{
				case DiceType.d2:
					return 1;
				case DiceType.d4:
					return 2;
				case DiceType.d6:
					return 3;
				case DiceType.d8:
					return 4;
				case DiceType.d10:
					return 5;
				case DiceType.d20:
					return 6;
			}
			return 1;
		}

		public static Property SelectProperty(PropertyRepository properties)
		{
			Console.WriteLine("Gespeicherte Eigenschaften");
			for (int i = 0; i < properties.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {properties.SortedItems[i].Name} ({properties.SortedItems[i].Id})");
			}
			var selectedProperty = GetNumber(0, "Wähle eine Eigenschaft", false);
			return (Property)properties.SortedItems[selectedProperty];
		}
	}
}