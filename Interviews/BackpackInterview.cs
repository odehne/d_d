﻿using DungeonsAndDragons.Classes;
using DungeonsAndDragons.Repositories;
using System;
using System.Collections.Generic;

namespace DungeonsAndDragons.Interviews
{

	public class BackpackInterview : Interview
	{
		public Pack ThePack { get; set; }

		public BackpackInterview()
		{
			ThePack = new Pack();
		}

		public BackpackInterview(Pack thePack)
		{
			ThePack = thePack;
		}
		public void RunInterview()
		{
			ThePack.Id = GetLine(ThePack.Id, "Name der Rucksack Zusammenstellung", "(engl.)");
			ThePack.Name = GetLine(ThePack.Name, "Name der Rucksack Zusammenstellung", "(deutsch)");
			ThePack.Items = GetPackItems(ThePack.Items);
		}

		private List<BackpackItem> GetPackItems(List<BackpackItem> items)
		{
			if (items == null)
				items = new List<BackpackItem>();
			Console.WriteLine("Vorhandene Rucksack Gegenstände");
			do
			{
				if (!YesNo("Einen weiteren Gegenstand hinzufügen"))
					return items;
				for (int i = 0; i < Program.BackpackItems.SortedItems.Count; i++)
				{
					Console.WriteLine($"{i + 1}. {Program.BackpackItems.SortedItems[i].Name}({Program.BackpackItems.SortedItems[i].Id})");
				}
				var seledtedItem = GetNumber(0, "Gegenstand wählen", false);
				var item = (BackpackItem)Program.BackpackItems.SortedItems[seledtedItem];
				if (item != null)
					item.Amount = GetNumber(1, "Welche Menge soll in den Rucksack", true);
				items.Add(item);
			} while (true);
		}
		internal static Pack SelectPack(PackRepository packs)
		{
			Console.WriteLine("Gespeicherte Rucksäcke");
			for (int i = 0; i < packs.SortedItems.Count; i++)
			{
				Console.WriteLine($"{i + 1}. {packs.SortedItems[i].Name} ({packs.SortedItems[i].Id})");
			}
			var selected = GetNumber(0, "Wähle einen Rucksack", false);
			return (Pack)packs.SortedItems[selected];
		}
	}
	
}