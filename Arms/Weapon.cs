using System.Collections.Generic;
using DungeonsAndDragons.Repositories;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;
using System.Text;
using System.Linq;

namespace DungeonsAndDragons.Arms
{


	public class Weapon : RepositoryItem<Weapon> {

        public string Description {get; set; } //melee weapon (martial, axe)
        public Categories Category {get; set; } //Items

		//Name		Cost    Damage			Weight  Properties
		//Battleaxe	10 gp	1d8 slashing	4 lbs	Versatile(1d10)

		public Damage DamageDie {get; set;} //1d8
		public DamageType DamageType { get; set; } //Thrown, Slashing
		public Damage AdditionalDamageDie { get; set; }
		public List<Property> WeaponProperties { get; set; } // Properties: Versatile

		public Rarity Rarity {get; set; } //Standard
        public int AttackBonus {get; set;}
        
        public double Weight {get; set; } // 4

		public Range Range { get; set; }

        public int Reach {get; set;} // Nahkampf
		public override string RepositoryFolder { get => "_weapons"; }

		public Cost Cost { get; set; }

		public WeaponClass Class { get; set; } 

		public Weapon()
		{
			Range = new Range();
			Cost = new Cost();
			WeaponProperties = new List<Property>();
			DamageDie = new Damage();
			Class = new WeaponClass("Einfache Nahkampfwaffen", "Simple Melee Weapons");
		}

		public override Weapon Load(string filename)
        {
            return JsonConvert.DeserializeObject<Weapon>(JsonLoader.LoadJsonFromFile(filename));
        }

		public override string ToString()
		{
			var sb = new StringBuilder();
			var dt = "";
			if (DamageType != null)
				dt = " " + DamageType.Name;
			sb.AppendLine("Name\tKosten\tSchaden\tGewicht\tEigenschaften");
			sb.AppendLine($"{Name}\t{Cost.ToString()}\t{DamageDie.ToString()}{dt}\t{Weight} lb.\t{GetPropertyNames()}");
			sb.AppendLine("Beschreibung: " + Description);
			return sb.ToString();
		}

		private string GetPropertyNames()
		{
			var ret = "";
			if (!WeaponProperties.Any())
				return "---";
			foreach (var prop in WeaponProperties)
			{
				ret = ret + prop.Name + ",";
			}
			return ret.TrimEnd(',');
		}
	} 

}

