using DungeonsAndDragons.Repositories;

namespace DungeonsAndDragons.Arms
{
    public class WeaponProficiency
    {
        public IRepositoryItem<Weapon> Weapon {get; set;}
        
        public WeaponProficiency(IRepositoryItem<Weapon> weapon)
        {
            Weapon = weapon;
        }

        public WeaponProficiency()
        {

        }

    }
}