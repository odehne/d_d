﻿using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace DungeonsAndDragons.Arms
{
	public enum CurrencyUnitTypes
	{
		Copper,
		Silver,
		Electrum,
		Gold,
		Platinum
	}

	public class CurrencyRepository : Repository<CurrencyUnit>
	{
		public const string FolderName = "_currencies";

		public override void Load()
		{
			var fils = GetFiles($"{RootFolder}\\{FolderName}");
			foreach (var fil in fils)
			{
				var itm = new CurrencyUnit();
				AddItem(itm.Load(fil));
			}
		}

		public CurrencyUnit GetUnit(CurrencyUnitTypes currencyType)
		{
			var lst = new List<CurrencyUnit>(Items.OfType<CurrencyUnit>());
			return lst.FirstOrDefault(x => x.UnitType.Equals(currencyType));
		}
	}

	public class CurrencyUnit : RepositoryItem<CurrencyUnit>
	{
		public CurrencyUnitTypes UnitType { get; set; }

		public override string RepositoryFolder => "_currencies";

		public string ShortName { get; set; }

		public double CpMultiplier { get; set; }
		public double SpMultiplier { get; set; }
		public double EpMultiplier { get; set; }
		public double GpMultiplier { get; set; }
		public double PpMultiplier { get; set; }
	
		public override CurrencyUnit Load(string filename)
		{
			return JsonConvert.DeserializeObject<CurrencyUnit>(JsonLoader.LoadJsonFromFile(filename));
		}

		public double CalculateExchangeRate(double value, CurrencyUnitTypes newRate)
		{
			//Coin				CP	SP	EP	GP	PP
			//Copper (cp)		1	10	50	100	1,000
			//Silver (sp)		1/10	1	5	10	100
			//Electrum (ep)		1/50	1/5	1	2	20
			//Gold (gp)			1/100	1/10	1/2	1	10
			//Platinum (pp)		1/1,000	1/100	1/20	1/10	1

			switch (newRate)
			{
				case CurrencyUnitTypes.Copper:
					return value * CpMultiplier;
				case CurrencyUnitTypes.Electrum:
					return value * EpMultiplier;
				case CurrencyUnitTypes.Silver:
					return value * SpMultiplier;
				case CurrencyUnitTypes.Gold:
					return value * GpMultiplier;
				case CurrencyUnitTypes.Platinum:
					return value * PpMultiplier;
			}
			return value;
		}
	}

	public class Cost
	{
		public double Value { get; set; }
		public CurrencyUnit Unit { get; set; }

		public override string ToString()
		{
			return $"{Value}{Unit.ShortName}";
		}
	}
}