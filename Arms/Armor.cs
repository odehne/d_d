using DungeonsAndDragons.Repositories;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;
using System.Collections.Generic;
using DungeonsAndDragons.Classes;
using System.Text;

namespace DungeonsAndDragons.Arms
{
	public class Armor : RepositoryItem<Armor>
    {
		public override string RepositoryFolder => "_armor";
		public ArmorClass Class{ get; set; } // Properties: light, medium, heavy, shield

		public Rarity Rarity { get; set; } //Standard
		public double Weight { get; set; } // 4

		public Cost Cost { get; set; }

		public string Description { get; set; }

		public HitPointsBasis BasePoints { get; set; }

		public bool StealthDisadvantage { get; set; }

		public int MinStrength { get; set; }

		public override Armor Load(string filename)
        {
           return JsonConvert.DeserializeObject<Armor>(JsonLoader.LoadJsonFromFile(filename));
        }

		public override string ToString()
		{
			var sb = new StringBuilder();
			sb.AppendLine("Name\tKosten\tRüstungsklasse\tStärke\tTarnung\tGewicht");
			sb.AppendLine($"{Name}\t{Cost.ToString()}\t{BasePoints.ToString()}{StealthDisadvantage}\t{Weight} lb.");
			sb.AppendLine("Beschreibung: " + Description);
			return sb.ToString();
		}
	}
}