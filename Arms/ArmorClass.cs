﻿using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;

namespace DungeonsAndDragons.Arms
{

	public class ArmorClass : RepositoryItem<ArmorClass>
	{
		public override string RepositoryFolder => "_armorClasses";

		public ArmorClass(string id, string name)
		{
			Id = id;
			Name = name;
		}

		public ArmorClass()
		{
		}

		public override ArmorClass Load(string filename)
		{
			return JsonConvert.DeserializeObject<ArmorClass>(JsonLoader.LoadJsonFromFile(filename));
		}
	}
}