﻿namespace DungeonsAndDragons.Arms
{
	public class Damage
	{
		public Die AttackDie { get; set; }
		public bool RollTwice { get; set; }
		public DamageType DamageType { get; set; }

		public Damage()
		{
			AttackDie = new Die(DiceType.d2);
		}

		public override string ToString()
		{
			var zwei = "";
			if (RollTwice)
				zwei = "2";
			return $"{zwei}{AttackDie.Type.ToString()}";
		}
	}
}