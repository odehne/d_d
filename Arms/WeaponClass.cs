﻿using DungeonsAndDragons.Repositories;
using Newtonsoft.Json;
using DungeonsAndDragons.Utilities;

namespace DungeonsAndDragons.Arms
{
	
	public class WeaponClass : RepositoryItem<WeaponClass>
	{
		public override string RepositoryFolder => "_weaponClasses";

		public WeaponClass(string name, string id)
		{
			Id = id;
			Name = name;
		}

		public WeaponClass()
		{
		}

		public override WeaponClass Load(string filename)
		{
			return JsonConvert.DeserializeObject<WeaponClass>(JsonLoader.LoadJsonFromFile(filename));
		}
	}

}

