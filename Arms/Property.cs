﻿using DungeonsAndDragons.Abilities;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DungeonsAndDragons.Arms
{
	public class Property : RepositoryItem<Property>
	{
		public override string RepositoryFolder => "_properties";
		public string Description { get; set; }
		public bool DamageDie { get; set; }
		public bool Range { get; set; }
		public override Property Load(string filename)
		{
			return JsonConvert.DeserializeObject<Property>(JsonLoader.LoadJsonFromFile(filename));
		}

		public Property()
		{
		}
	}
}

