using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace DungeonsAndDragons.Arms
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ItemRarity {
		Common,
		Uncommon,
		Rare,
		VeryRare,
		Legendary
	}

}

