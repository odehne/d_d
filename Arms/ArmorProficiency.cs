using DungeonsAndDragons.Repositories;

namespace DungeonsAndDragons.Arms
{

    public class ArmorProficiency
    {
        public IRepositoryItem<Armor> Armor {get; set;}

        public ArmorProficiency(IRepositoryItem<Armor> armor)
        {
            Armor = armor;
        }
    }    
}

