﻿using System;
using DungeonsAndDragons.Repositories;
using DungeonsAndDragons.Utilities;
using Newtonsoft.Json;

namespace DungeonsAndDragons.Arms
{

	public class DamageType : RepositoryItem<DamageType>
	{
		public override string RepositoryFolder => "_damagetypes";

		public override DamageType Load(string filename)
		{
			return JsonConvert.DeserializeObject<DamageType>(JsonLoader.LoadJsonFromFile(filename));
		}

		public override string ToString()
		{
			return Name;
		}
	}

}

